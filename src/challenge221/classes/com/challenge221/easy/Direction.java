package com.challenge221.easy;

public enum Direction {
    UP(0),
    RIGHT(1),
    DOWN(2),
    LEFT(3);

    private final int value;

    private Direction(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public String toString(Direction d) {
        switch (d) {
            case UP:
                return "up";
            case DOWN:
                return "down";
            case LEFT:
                return "down";
            default:
                return "right";
        }
    }
}
