package com.challenge221.easy;

import java.util.Random;

public class WordSnake {

    StringBuilder sb = new StringBuilder();
    private static final Random rand = new Random();
    public static char[][] array;
    private static final String[] sample_input = new String[]{
        "DELOREAN", "NEUTER", "RAMSHACKLE", "EAR", "RUMP", "PALINDROME", "EXEMPLARY", "YARD"
    };
    private static Direction[] directions;

    public static int getAbs(int number) {
        if (number < 0) {
            return -number;
        } else {
            return number;
        }
    }

    /**
     * Return direction changed by 90° or -90° Return value is re
     *
     * @param lastDirection direction of last word
     * @param currentPosition position in array
     * @param index current word from input
     * @return
     */
    public static Direction turn(Direction lastDirection, int[] currentPosition, int index) {
        int random = rand.nextInt(10) % 2;

        switch (lastDirection) {
            case UP:
                if (random == 0) {
                    // Make sure current Position never undershoots 0
                    if (currentPosition[1] - sample_input[index].length() < 0) {
                        return Direction.RIGHT;
                    } else {
                        return Direction.LEFT;
                    }
                } else {
                    return Direction.RIGHT;
                }
            case RIGHT:
                if (random == 0) {
                    // Make sure current Position never undershoots 0
                    if (currentPosition[0] - sample_input[index].length() < 0) {
                        return Direction.DOWN;
                    } else {
                        return Direction.UP;
                    }
                } else {
                    return Direction.DOWN;
                }
            case DOWN:
                if (random == 0) {
                    return Direction.RIGHT;
                } else // Make sure current Position never undershoots 0
                if (currentPosition[1] - sample_input[index].length() < 0) {
                    return Direction.RIGHT;
                } else {
                    return Direction.LEFT;
                }
            default:
                if (random == 0) {
                    return Direction.DOWN;
                } else if (currentPosition[0] - sample_input[index].length() < 0) {
                    return Direction.DOWN;
                } else {
                    return Direction.UP;
                }
        }
    }

    public static int[] getFirstDirection(int[] currentPosition) {
        int number = rand.nextInt(10);
        if (number % 2 == 0) {
            directions[0] = Direction.RIGHT;
            return new int[]{0, sample_input[0].length()};
        } else {
            directions[0] = Direction.DOWN;
            return new int[]{sample_input[0].length(), 0};
        }
    }

    public static int[] calcArraySize() {
        // UP,RIGHT,DOWN,LEFT
        int[] maxima = new int[]{0, 0};
        int[] currentPosition = new int[]{0, 0};

        for (int i = 0; i < sample_input.length; i++) {

            if (i != 0) {
                directions[i] = turn(directions[i - 1], currentPosition, i);
            } else {
                currentPosition = getFirstDirection(currentPosition);
            }

            switch (directions[i]) {
                case UP:
                    currentPosition[0] -= sample_input[i].length();
                    break;
                case DOWN:
                    currentPosition[0] += sample_input[i].length();
                    if (currentPosition[0] > maxima[0]) {
                        maxima[0] = currentPosition[0];
                    }
                    break;
                case LEFT:
                    currentPosition[1] -= sample_input[i].length();
                    break;
                default:
                    currentPosition[1] += sample_input[i].length();
                    if (currentPosition[1] > maxima[0]) {
                        maxima[0] = currentPosition[0];
                    }
                    break;
            }
        }

        // Calculate width and heigth from differences to edges        
        return maxima;
    }

    public static void makeSnake() {
        String[] output;
        int[] arraySize = new int[2];
        directions = new Direction[sample_input.length]; // Initialize directions array based on input
        int[] currentPosition = new int[]{0, 0};

        arraySize = calcArraySize(); // Start with random direction 

        for (int i = 0; i < sample_input.length; i++) {
            int length = sample_input[i].length();

            switch (directions[i]) {
                case UP:
                    snakeUp(i, currentPosition);
                    break;
                case RIGHT:
                    snakeRight(i, currentPosition);
                    break;
                case DOWN:
                    snakeDown(i, currentPosition);
                    break;
                default:
                    snakeLeft(i, currentPosition);
                    break;
            }
        }
    }

    public static int[] snakeUp(int index, int[] currentPosition) {
        for (int i = sample_input[index].length(); i > 0; i--) {
            array[currentPosition[0] - i][currentPosition[1]] = sample_input[index - i].charAt(currentPosition[0]);
        }
        currentPosition[0] -= sample_input[index].length() - 1;

        return currentPosition;
    }

    public static int[] snakeRight(int index, int[] currentPosition) {
        for (int i = 0 - 1; i < sample_input[index].length(); i++) {
            array[currentPosition[0]][currentPosition[1] + i] = sample_input[index].charAt(currentPosition[1] + i);
        }
        currentPosition[0] -= sample_input[index].length() - 1;

        return currentPosition;
    }

    public static int[] snakeDown(int index, int[] currentPosition) {
        for (int i = sample_input[index].length() - 1; i > 0; i--) {
            array[currentPosition[0] - 1][currentPosition[1]] = sample_input[index].charAt(i);
        }
        currentPosition[0] -= sample_input[index].length() - 1;

        return currentPosition;
    }

    public static int[] snakeLeft(int index, int[] currentPosition) {
        for (int i = sample_input[index].length() - 1; i > 0; i--) {
            array[currentPosition[0] - 1][currentPosition[1]] = sample_input[index].charAt(i);
        }
        currentPosition[0] -= sample_input[index].length() - 1;

        return currentPosition;
    }

    public static void main(String[] args) {
        makeSnake();
    }

}
