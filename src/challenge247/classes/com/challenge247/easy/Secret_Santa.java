package com.challenge247.easy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Random;

public class Secret_Santa {

    public static List<Pair<String, String>> randomMatch(List<String> presenter, List<String> presentee) {

        Random random = new Random();                   //Create randomizer to extract random element from list
        List<Pair<String, String>> pairs = new ArrayList<>(presenter.size()); // Create pair list the size of the individual elements to be paired 
        Pair<String, String> element = new Pair(null, null); //Create object to match elements then add to pair list
        StringBuilder sb = new StringBuilder();         //Create Stringbuilder for output of result

        System.out.println("-----Pairs-----");
        presenter.forEach((temp) -> {

            element.setL(temp);
            int rand = random.nextInt(presentee.size());

            if (element.getL().equals(presentee.get(rand))) {
                if (presentee.size() == 2) // Case: Random element is second last option and makes a pair of equal values
                {
                    element.setR(presentee.remove(rand + 1 % (presentee.size() - 1))); // Take next element (modulo to stay within list bound)
                } else {
                    element.setR(presentee.remove(1));
                }
            } else {
                element.setR(presentee.remove(rand));
            }

            sb.append(element.getL()).append(" -> ").append(element.getR());
            System.out.println(sb);
            sb.setLength(0);
            pairs.add(element);
        });

        return pairs;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Collection names = Arrays.asList(args);
        Collection names = Arrays.asList("Sean",
                "Winnie",
                "Brian Amy",
                "Samir",
                "Joe Bethany",
                "Bruno Anna Matthew Lucas",
                "Gabriel Martha Philip",
                "Andre",
                "Danielle",
                "Leo Cinthia",
                "Paula",
                "Mary Jane",
                "Anderson",
                "Priscilla",
                "Regis Julianna Arthur",
                "Mark Marina",
                "Alex Andrea");
        ArrayList<String> presenter = new ArrayList<>(names);
        ArrayList<String> presentee = new ArrayList<>(names);

        System.out.println("presenters:" + names);
        // Assign randomized pairs
        List<Pair<String, String>> pairList = randomMatch(presenter, presentee);
    }

}
