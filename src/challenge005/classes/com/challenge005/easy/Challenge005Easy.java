package com.challenge005.easy;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Your challenge for today is to create a program which is password protected, and wont open unless the correct user
 * and password is given.
 *
 * For extra credit, have the user and password in a seperate .txt file. for even more extra credit, break into your own
 * program :)
 *
 * @author pr
 */
public class Challenge005Easy {

    private static String USER = "admin";
    private static String PASS = "123";

    private static void commenceHacking() {
    }

    public static void main(String[] args) {
        // get username and password from file
        if (args.length == 1) {
            try {
                Scanner s = new Scanner(new File(args[0]));
                USER = s.nextLine();
                PASS = s.nextLine();

            } catch (FileNotFoundException e) {
            }

        }

        Scanner s = new Scanner(System.in);
        String u = "";
        String p = "";
        int triesLeft = 3;

        System.out.println("Enter username and password");
        while (true) {
            if (triesLeft < 1) {
                System.out.println("intruder alert, charging capSLOCK TO DETER ENEMY! SHUTTING DOWN!");
                break;
            }

            System.out.print("username: ");
            u = s.nextLine();
            System.out.print("password: ");
            p = s.nextLine();

            if (!u.equals(USER)) {
                triesLeft--;
                System.out.println(String.format("Wrong Username, %s tries remain", triesLeft));
            } else if (!p.equals(PASS)) {
                triesLeft--;
                System.out.println(String.format("Wrong Password, %s tries remain", triesLeft));
            } else {
                System.out.println("Thank you for breaching this program and gracing its memory, oh great hacker.");
                commenceHacking();
                break;
            }
        }
    }
}
