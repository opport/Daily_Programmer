package com.challenge005.medium;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Your challenge today is to write a program that can find the amount of anagrams within a .txt file. For example,
 * "snap" would be an anagram of "pans", and "skate" would be an anagram of "stake".
 *
 * @author pr
 */
public class Challenge005Medium {

    private static final HashSet<String> wordsFound = new HashSet<>();

    /**
     * Iterate through all words found and remove those that are anagrams of given string.
     *
     * @param str String to be matched
     */
    private static void findAnagram(String str) {

        List<String> foundWords = new ArrayList<>();

        for (String s : wordsFound) {
            if (s.length() == str.length() && !s.equals(str)) {

                StringBuilder sb = new StringBuilder(s);

                for (int i = 0; i < str.length(); i++) {
                    if (s.indexOf(str.charAt(i)) > -1) {
                        sb.deleteCharAt(i);
                    }
                }

                if (sb.length() == 0) {
                    foundWords.add(sb.toString());
                }
            }
        }

        // finally addword being checked to words to be removed
        if (foundWords.size() > 0) {
            foundWords.add(str);
        }

        wordsFound.removeAll(foundWords);
    }

    public static void main(String[] args) throws IOException {
        if (args.length == 0) {
            return;
        }

        try {
            // get rid of any special characters
            String[] words = Files.readString(Paths.get(args[0])).replaceAll("[^A-Za-z0-9\\-]+", " ").split("\\s+|\\W+");

            for (String word : words) {
                wordsFound.add(word);
            }

            List<String> anagrams = new ArrayList<>();

            for (String s : wordsFound) {
                findAnagram(s);
            }

        } catch (FileNotFoundException e) {
            System.out.println(String.format("Could not find file with the name \"%s\"", args[0]));
        }
    }
}
