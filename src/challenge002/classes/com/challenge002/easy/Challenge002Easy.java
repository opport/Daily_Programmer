package com.challenge002.easy;

import java.util.Scanner;

/**
 * Hello, coders! An important part of programming is being able to apply your programs, so your challenge for today is
 * to create a calculator application that has use in your life. It might be an interest calculator, or it might be
 * something that you can use in the classroom. For example, if you were in physics class, you might want to make a F =
 * M * A calc. EXTRA CREDIT: make the calculator have multiple functions! Not only should it be able to calculate F = M
 * * A, but also A = F/M, and M = F/A!
 *
 * @author pr
 */
public class Challenge002Easy {

    private static Scanner s = new Scanner(System.in);

    private static int add(int a, int b) {
        return a + b;
    }

    private static int subtract(int a, int b) {
        return a - b;
    }

    private static int multiply(int a, int b) {
        return a * b;
    }

    private static int divide(int a, int b) {
        return a / b;
    }

    public static void main(String[] args) {
        while (true) {
            System.out.println("Enter Expression a + b, a - b, a * b or a / b \n or 0 to exit");
            String exp = s.nextLine();

            if (exp.equals("0")) {
                break;
            }

            exp = exp.replaceAll("\\s+", "");
            String[] split = exp.split("(\\+|\\-|\\*|\\/)");

            int a, b;

            try {
                a = Integer.parseInt(split[0]);
                b = Integer.parseInt(split[1]);

                int result;

                if (exp.contains("+")) {
                    result = add(a, b);
                } else if (exp.contains("-")) {
                    result = subtract(a, b);
                } else if (exp.contains("*")) {
                    result = multiply(a, b);
                } else if (exp.contains("/")) {
                    result = divide(a, b);
                } else {
                    result = a;
                }

                System.out.println(String.format("%s = %s ", exp, result));
            } catch (NumberFormatException e) {
                System.out.println(e);
            }
        }
    }
}
