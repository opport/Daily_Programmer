package com.challenge002.hard;

import java.util.Scanner;

/**
 * Your mission is to create a stopwatch program. this program should have start, stop, and lap options, and it should
 * write out to a file to be viewed later.
 *
 * @author pr
 */
public class Challenge002Hard {

    private final static Scanner s = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Choose option:");

        while (true) {
            System.out.println("1. Start");
            System.out.println("2. Add lap (while running)");
            System.out.println("Else Stop");

            String option = s.nextLine();

            if (option.equals("1")) {
                Log l = new Log();
                l.start();

                while (true) {
                    String button = s.nextLine();

                    if (button.equals("2")) { // lap
                        l.addEntry();
                    }
                    if (button.equals("3")) { // add last entry and exit
                        l.addEntry();
                        System.out.println(l.toString());
                        break;
                    }
                }
            } else {
                return;
            }
        }
    }
}
