package com.challenge002.hard;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pr
 */
public class Log {

    private long t;
    private final SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
    List<Long> entries = new ArrayList<>();

    public Log() {
    }

    public void start() {
        t = System.currentTimeMillis();
    }

    public void addEntry() {
        long currentTime = System.currentTimeMillis();
        entries.add(currentTime);
    }

    @Override
    public String toString() {

        String result = "In mm:ss:milli\n#\tTotal\t\tLap time\n";

        long lastTime = entries.isEmpty()
                ? 0
                : t;

        for (int i = 0; i < entries.size(); i++) {

            Duration elapsedTotal = Duration.ofMillis(entries.get(i) - t);
            Duration elapsedLast = Duration.ofMillis(entries.get(i) - lastTime);

            lastTime = entries.get(i);

            //long tHours = elapsedTotal.getSeconds() / 3600;
            long tMinutes = elapsedTotal.toMinutes();
            long tSeconds = elapsedTotal.toSeconds();
            long tMilliSeconds = elapsedTotal.toMillis() % 1000;

            //long lHours = elapsedLast.getSeconds() / 3600;
            long lMinutes = elapsedLast.toMinutes();
            long lSeconds = elapsedLast.toSeconds();
            long lMilliSeconds = elapsedLast.toMillis() % 1000;

//            String total = String.format("%d\t%02d:%02d:%02d", i, tHours, tMinutes, tSeconds);
//            String last = String.format("%02d:%02d:%02d", lHours, lMinutes, lSeconds);
            String total = String.format("%d\t%02d:%02d:%03d", i, tMinutes, tSeconds, tMilliSeconds);
            String last = String.format("%02d:%02d:%03d", lMinutes, lSeconds, lMilliSeconds);

            result += String.format("%s \t%s\n", total, last);
        }

        return result;
    }
}
