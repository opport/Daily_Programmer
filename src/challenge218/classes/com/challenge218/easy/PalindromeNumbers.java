package com.challenge218.easy;

import com.challenge229.hard.DivisibleBySeven;
import java.util.*;

public class PalindromeNumbers {

    public static <T> boolean isPalindrome(T input) {

        char[] num_array = input.toString().toCharArray();
        int array_length = num_array.length;

        for (int i = 0; i < (num_array.length / 2); i++) {
            if (num_array[i] != num_array[array_length - i - 1]) {
                return false;
            }
        }

        return true;
    }

    public static List<Integer> palindromize(int[] numbers) {
        List<Integer> palindromes = new ArrayList<>();

        for (int i : numbers) {
            while (!isPalindrome(i)) {
                i += DivisibleBySeven.reverse(i);
            }
            palindromes.add(i);
        }

        return palindromes;
    }

    public static void main(String[] args) {
        int[] sample_input = {24, 28};
        System.out.println("Input: " + Arrays.toString(sample_input));
        System.out.println("Output:" + palindromize(sample_input));
    }

}
