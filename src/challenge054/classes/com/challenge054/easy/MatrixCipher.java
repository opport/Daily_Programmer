
package com.challenge054.easy;

import java.util.Random;
import java.util.Scanner;

/**
 * @author PR
 * @version 1.0
 */
public class MatrixCipher {
    
    /**
     * Width key to processString text
     */
    private static int key = 0;
    
    /**
     * Array to save encrypted text to
     */
    static char[][] encryptedText;
    
    /**
     * Randomizer to add random letters when text doesn't quite match the array
     */
    private static final Random random = new Random();
    
    /**
     * Scanner for text input 
     */
    private static final Scanner scanner= new Scanner(System.in);
    
    /**
     * Encrypt string using a key to describe the line length in the array.
     * processString(string) decides which text is supposed to be encrypted, 
     * calling initializeArray(string) to create a storage array based on the string
     * followed by the actual encryption in encrypt(string).
     * @param args Custom string to be encrypted
     */
    public static void main(String[] args) {
        // Check if a special text was given to be encrypted
        if (args.length > 0) {
            processString(args[0]);
        } else {    // Default text to be encrypted
            processString("The cake is a lie!");
        }
    }
    
    /**
     * Template method for the step-by-step encryption process
     * @param string String to be encrypted
     */
    public static void processString(String string) {
        // Check if there is anything to be processed
        if (string.equals("") || string.isEmpty()) {
            System.out.println("No text to be encrypted");
        }
        
        initializeArray(string);
        encrypt(string);
        printArray();
        printCipherText();
    }
    
    /**
     * Encryption by copying text to array
     * @param string String to be encrypted
     */
    public static void encrypt(String string) {
        // Use index to keep track of string char to be copied to array
        int index = 0;
        
        for (int i = 0; i < encryptedText.length; i++) {
            for (int j = 0; j < key; j++) {
                // Find out if all characters in string have been copied
                if (index < string.length()) {
                    if (string.charAt(index) == ' ') {
                        encryptedText[i][j] = '_';
                        index++;
                    } else {
                        encryptedText[i][j] = string.charAt(index);
                        index++;
                    }
                } else {    // add random characters until end of line is reached
                        encryptedText[i][j] = (char)(random.nextInt(127));
                        index++;
                }
            }
        }
    }
    
    /**
     * Create array to store text in after encryption
     * @param string String to be encrypted
     */
    public static void initializeArray(String string) {
        System.out.println("Choose key to encrypt with.");
        //key = 7;
        while (key < 1) {
            key = scanner.nextInt();
        }
        
        // Get rows by dividing string length by key and round up for remainder
        int rows = (string.length() % key) == 0 ? 
                    (string.length() / key) : (string.length() / key)+1;
        
        encryptedText = new char[rows][key];
    }
    
    /**
     * Print resulting array
     */
    public static void printArray() {
        System.out.println("Resulting array:");
        for (int i = 0; i < encryptedText.length; i++) {
            for (int j = 0; j < key; j++) {
                System.out.print(encryptedText[i][j] + " ");
            }
            System.out.println("");
        }
    }
    
    /**
     * Print resulting text by column
     */
    public static void printCipherText() {
        System.out.println("Ciphertext:");
        for (int i = 0; i < key; i++) {
            for (int j = 0; j < encryptedText.length; j++) {
                System.out.print(encryptedText[j][i]);
            }
            System.out.println("");
        }
    }
}
