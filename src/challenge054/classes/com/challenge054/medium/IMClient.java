
package com.challenge054.medium;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

/**
 * @author PR
 * @version 1.0
 */
public class IMClient {
    public static void main(String[] args) throws IOException {
        Socket client = new Socket("localhost",18000);
        
        BufferedReader clientInput  = new BufferedReader(new InputStreamReader(client.getInputStream()));
        PrintStream clientOutput  = new PrintStream( client.getOutputStream(),true );
        BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
        
        String userInput;
        do {
            userInput = stdIn.readLine();
            clientOutput.println(userInput);
            userInput = clientInput.readLine();
            System.out.println("Server: " + userInput);
        } while (!(userInput.equalsIgnoreCase("bye")));
    }
}
