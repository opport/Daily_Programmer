
package com.challenge054.medium;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author PR
 * @version 1.0
 */
public class IMServer {
    
    
    public static void main(String[] args) throws IOException {
        ServerSocket server = new ServerSocket(18000);
        
        Socket client;
        
        
        while (true) {
            client = server.accept();
            
            BufferedReader serverInput  =  new BufferedReader(new InputStreamReader(client.getInputStream()));
            PrintStream serverOutput  = new PrintStream( client.getOutputStream(),true );
            BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
            
            String userInput;
            do {
                userInput = serverInput.readLine();
                System.out.println("Client:" + userInput);
                userInput = stdIn.readLine();
                serverOutput.println(userInput);
            } while (!(userInput.equalsIgnoreCase("bye")));
        }
        
        
    }
}
