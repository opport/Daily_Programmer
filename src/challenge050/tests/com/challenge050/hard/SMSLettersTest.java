package com.challenge050.hard;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 *
 * @author PR
 */
public class SMSLettersTest {

    public SMSLettersTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of isDigit method, of class SMSLetters.
     */
    @Test
    public void testIsDigit() {
        System.out.println("isDigit");
        String str = "14";
        boolean expResult = false;
        boolean result = SMSLetters.isDigit(str);
        assertEquals(expResult, result);
    }

    /**
     * Test of toLetter method, of class SMSLetters.
     */
    @Test
    public void testToLetter() {
        System.out.println("toLetter");
        String str = "44";
        SMSLetters.toLetter(str);
        fail("The test case is a prototype.");
    }

    /**
     * Test of parse method, of class SMSLetters.
     */
    @Test
    public void testParse() {
        System.out.println("parse");
        String str = "";
        SMSLetters.parse(str);
        fail("The test case is a prototype.");
    }

    /**
     * Test of main method, of class SMSLetters.
     */
    @Test
    public void testMain() {
        System.out.println("main");
        String[] args = null;
        SMSLetters.main(args);
        fail("The test case is a prototype.");
    }

}
