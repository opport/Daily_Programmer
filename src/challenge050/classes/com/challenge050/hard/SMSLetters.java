package com.challenge050.hard;

import java.util.HashMap;
import java.util.Map;

public class SMSLetters {

    private static final StringBuilder sb = new StringBuilder();
    private static final Map<String, Character> letters;

    static {//2=ABC, 3=DEF, 4=GHI, 5=JKL, 6=MNO, 7=PQRS, 8=TUV, 9=WXYZ
        letters = new HashMap<>();
        letters.put("2", 'a');
        letters.put("22", 'b');
        letters.put("222", 'c');
        letters.put("3", 'd');
        letters.put("33", 'e');
        letters.put("333", 'f');
        letters.put("4", 'g');
        letters.put("44", 'h');
        letters.put("444", 'i');
        letters.put("5", 'j');
        letters.put("55", 'k');
        letters.put("555", 'l');
        letters.put("6", 'm');
        letters.put("66", 'n');
        letters.put("666", 'o');
        letters.put("7", 'p');
        letters.put("77", 'q');
        letters.put("777", 'r');
        letters.put("7777", 's');
        letters.put("8", 't');
        letters.put("88", 'u');
        letters.put("888", 'v');
        letters.put("9", 'w');
        letters.put("99", 'x');
        letters.put("999", 'y');
        letters.put("9999", 'z');
        letters.put("0", ' ');
    }

    public static boolean isDigit(String str) {
        char[] ch = str.toCharArray();

        for (int i = 1; i < ch.length; i++) {
            if (ch[i] != ch[i - 1]) {
                return false;
            }
        }
        return true;
    }

    public static void toLetter(String str) {
        if (isDigit(str)) {
            System.out.print(letters.get(str));
        } else {
            System.err.println(str + " is not a valid number.");
        }

    }

    public static void parse(String str) {
        char[] input = str.toCharArray();

        for (int i = 0; i < input.length; i++) {

            // Once 4 characters have been collected in a row;
            if (sb.length() > 3) {
                toLetter(sb.toString());    // No match
                sb.setLength(0);    // Prepare for next digit
                i += 2; // Make i too big for outer loop to continue
                continue;
            } else {
                sb.append(input[i]);
            }

            for (int j = 1; j < 4; j++) {
                if (i + j >= input.length) { // Out of bounds
                    toLetter(sb.toString()); // No match
                    sb.setLength(0);    // Prepare for next digit
                    i += j; // Make i too big for outer loop to continue
                    break;
                } else if (input[i + j] == ' ') { // Separator
                    toLetter(sb.toString());
                    sb.setLength(0);            // Prepare for next digit
                    i += j;                       // Skip over whitespace
                    break;
                } else if (input[i + j] == '0') { // Zero = Space
                    toLetter(sb.toString());
                    sb.setLength(0);            // Prepare for next digit
                    i += j;                       // Skip over whitespace
                    System.out.print(' ');
                    break;
                } else if (sb.charAt(0) == input[i + j]) {  // Match
                    sb.append(input[i + j]);
                } else {
                    toLetter(sb.toString());    // No match
                    sb.setLength(0);            // Prepare for next digit
                    i += j - 1;                     // Update index for input letters
                    break;
                }
            }
        }
        System.out.println("");
    }

    public static void main(String[] args) {
        String sample_input = "999337777";
        parse(sample_input);
    }
}
