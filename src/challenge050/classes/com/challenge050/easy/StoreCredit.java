package com.challenge050.easy;

import java.util.Arrays;
import java.util.Scanner;

public class StoreCredit {

    private static int CREDIT;
    private static int[] item_options;
    private static int[] final_items;
    private static final Scanner sc = new Scanner(System.in);

    public static void toIntArray(String[] input) {
        item_options = new int[input.length];

        for (int i = 0; i < input.length; i++) {
            item_options[i] = Integer.parseInt(input[i]);
        }
    }

    public static void getOption() {
        for (int i = 0; i < item_options.length; i++) {
            if (item_options[i] <= CREDIT && item_options[i] >= 0) {
                for (int j = i; j < item_options.length; j++) {
                    if (item_options[i] + item_options[j] == CREDIT) {
                        final_items = new int[]{item_options[i], item_options[j]};
                        return;
                    }
                }
            }
        }
        System.out.println("No matching pair found!");
    }

    public static void main(String[] args) {
        System.out.println("Set credit: ");
        CREDIT = sc.nextInt();
        sc.nextLine();
        System.out.println("Enter item options: ");
        String options_input = sc.nextLine();
        toIntArray(options_input.split("\\s"));
        getOption();
        System.out.println(Arrays.toString(final_items));
    }
}
