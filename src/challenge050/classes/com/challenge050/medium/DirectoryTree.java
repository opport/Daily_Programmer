package com.challenge050.medium;

import java.io.IOException;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class DirectoryTree {

    private static int DEPTH;
    private final static Scanner sc = new Scanner(System.in);

    public static void printSubDirectories(Path dir, int depth) {
        if (depth > DEPTH) {
            return;
        } else if (Files.isDirectory(dir)) {
            if (depth == 0) {
                System.out.println(dir.toString());
            } else {
                System.out.println("\\" + dir.getFileName());
            }
        } else {
            System.out.println(dir.getFileName());
        }

        try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir)) {
            for (Path file : stream) {
                if (Files.isRegularFile(file)) {
                    System.out.println(file.getFileName());
                } else {
                    printSubDirectories(file, depth + 1);
                }
            }
        } catch (IOException | DirectoryIteratorException x) {
            // IOException can never be thrown by the iteration.
            // In this snippet, it can only be thrown by newDirectoryStream.
            System.err.println(x);
        }
    }

    public static void main(String[] args) {
        //System.out.println("Enter Path to be printed: ");
        //Path dir = Paths.get(sc.nextLine());
        Path dir = Paths.get("C:\\Intel");
        System.out.println("Enter max sub directory depth: ");
        DEPTH = sc.nextInt();

        printSubDirectories(dir, 0);
    }
}
