package com.challenge230.easy;

import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.JSONValue;
import org.json.simple.parser.ParseException;
import org.json.simple.parser.JSONParser;

public class JSONTreasureHunt {

    public static void main(String[] args) {
        JSONParser parser = new JSONParser();
        Object sample_input = new JSONObject();
        sample_input = JSONValue.parse("[0,{\"1\":{\"2\":{\"3\":{\"4\":[5,{\"6\":7}]}}}}]");
        System.out.println(sample_input);
    }
}
