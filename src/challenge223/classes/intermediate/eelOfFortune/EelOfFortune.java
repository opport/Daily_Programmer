package challenge223.intermediate.eelOfFortune;

import java.util.*;

public class EelOfFortune {

    /**
     *
     * @param s
     * @return
     */
    public static Set<Character> getLetters(String s) {
        Set<Character> letters = new HashSet<>();
        for (int i = 0; i < s.length(); i++) {
            letters.add(s.charAt(i));
        }

        return letters;
    }

    public static boolean evaluate(String slur, String[] input) {
        String slurLow = slur.toLowerCase();    // To lowercase for evaluation purposes
        Set<Character> letters = new HashSet<>(getLetters(slur)); // set to find if letter appears where it shouldn't
        boolean match = false;
        int index_input = 0;    // Last evaluated letter in the slur's letter order 

        for (String s : input) {
            String wordLow = s.toLowerCase();

            for (int i = 0; i < slurLow.length(); i++) {
                for (int j = index_input; j < wordLow.length(); j++) {

                    // Letter in slur is found in wrong order
                    if (slurLow.charAt(i) != wordLow.charAt(j) && letters.contains(wordLow.charAt(j))) {
                        match = false;
                        break;
                    } else if (slurLow.charAt(i) == wordLow.charAt(j)) { // Letter found in right order in loop
                        match = true;
                        index_input = j + 1;
                        break;
                    }
                    if (j == wordLow.length() - 1 && letters.contains(wordLow.charAt(j)) == false) { // No matching letter
                        match = false;
                        break;
                    }
                }

                if (match == false || index_input == wordLow.length()) {
                    index_input = 0;
                    break;
                }
            }
            //problem("snond", "snond") -> true
            System.out.println("problem(\"" + s + "\", \"" + slur + "\") -> " + match);
        }

        return match;
    }

    public static void main(String[] args) {
        String[] sample_input = {"Synchronized", "misfunctioned", "mispronounced", "shotgunned", "snond"};
        String sample_slur = "snond";
        System.out.println(evaluate(sample_slur, sample_input));
        //System.out.println(getLetters(sample_slur).contains('d'));
    }
}
