package com.challenge004.easy;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

/**
 * You're challenge for today is to create a random password generator!
 *
 * For extra credit, allow the user to specify the amount of passwords to generate.
 *
 * For even more extra credit, allow the user to specify the length of the strings he wants to generate!
 *
 * @author pr
 */
public class Challenge004Easy {

    private final static Random RAND = new Random();
    private final static Scanner SCAN = new Scanner(System.in);
    private final static int ASCII_MAX = 127;

    public static void main(String[] args) {

        System.out.println("How many passwords shall be generated?");
        int number = SCAN.nextInt();
        SCAN.nextLine(); // clear buffer
        System.out.println("How long shall password be?");
        int length = SCAN.nextInt();
        SCAN.nextLine(); // clear buffer

        StringBuilder sb = new StringBuilder();
        List<String> passwords = new ArrayList<>(number);

        for (int i = 0; i < number; i++) {
            for (int j = 0; j < length; j++) {

                int random = Math.max((Math.abs(RAND.nextInt()) % ASCII_MAX), 32); // Make sure value is positive and >= 32
                sb.append((char) random);
            }

            passwords.add(sb.toString());
            sb.setLength(0); // clear string builder for next password
        }

        System.out.println("Generated passwords:");
        passwords.forEach(s -> System.out.println(String.format("%s\tlength = %s", s, s.length())));
    }
}
