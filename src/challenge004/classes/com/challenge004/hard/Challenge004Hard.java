package com.challenge004.hard;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * today, your challenge is to create a program that will take a series of numbers (5, 3, 15), and find how those
 * numbers can add, subtract, multiply, or divide in various ways to relate to eachother. This string of numbers should
 * result in 5 * 3 = 15, or 15 /3 = 5, or 15/5 = 3. When you are done, test your numbers with the following strings:
 *
 * 4, 2, 8 6, 2, 12 6, 2, 3 9, 12, 108 4, 16, 64
 *
 * For extra credit, have the program list all possible combinations. for even more extra credit, allow the program to
 * deal with strings of greater than three numbers. For example, an input of (3, 5, 5, 3) would be 3 * 5 = 15, 15/5 = 3.
 * When you are finished, test them with the following strings.
 *
 * 2, 4, 6, 3 1, 1, 2, 3 4, 4, 3, 4 8, 4, 3, 6 9, 3, 1, 7
 *
 * @author pr
 */
public class Challenge004Hard {

//    private static List<String> getCombinations(List<Integer> numbers, List<String> results){
//        for (List<Integer> list : numbers) {
//            list.forEach(action);
//        }
//    }
//    
//    private static void getCombinations(List<List<Integer>> numbers, List<String> results){
//        for (List<Integer> list : numbers) {
//            list.forEach(action);
//        }
//    }
    public static void main(String[] args) {
        List<List<Integer>> numbers = new ArrayList<>();
        numbers.add(Arrays.asList(4, 2, 8));
        numbers.add(Arrays.asList(6, 2, 12));
        numbers.add(Arrays.asList(6, 2, 3));
        numbers.add(Arrays.asList(9, 12, 108));
        numbers.add(Arrays.asList(4, 16, 64));

    }
}
