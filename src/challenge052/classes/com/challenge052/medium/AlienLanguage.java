package com.challenge052.medium;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class AlienLanguage {
    private static int LETTERCOUNT;
    private static int WORDS;
    private static Set<Character> alphabet = new HashSet<>();
    
    public static char toUpper(Character ch){
        return (char)(((int) 'A' -(int) 'a')+(int) ch);
    }

    public static void extractLetters(String input){
        ArrayList<Character> result = new ArrayList<>();

        for (int i = 0; i < input.length(); i++) {
            if (input.charAt(i) == '(') {
                i++;
                while (input.charAt(i) != ')') {
                    // Add missing letters to alphabet
                    if (alphabet.contains(input.charAt(i)) == false) {
                        alphabet.add(input.charAt(i));
                    }
                    // Add uppercase char to list for final evaluation
                    result.add(toUpper(input.charAt(i)));
                    
                    i++;
                }
            } else {
                // Add missing letters to alphabet
                if (alphabet.contains(input.charAt(i)) == false) {
                    alphabet.add(input.charAt(i));
                } 
                result.add(input.charAt(i));
            }
        }
        
        decipher(result.toString(),0);
    }
    
    public static int decipher(String input, int index){
        int result = 0;        
        for (int i = index; i < input.length(); i++) {
            
        }
        return 0;
    }
    
    public static void main(String[] args) {
        String sample_input = "(ab)d(dc)";
        
        extractLetters(sample_input);
    }
}
