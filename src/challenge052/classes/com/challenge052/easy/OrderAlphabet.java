package com.challenge052.easy;

import java.util.HashMap;
import java.util.Scanner;

public class OrderAlphabet {
    private static final Scanner sc = new Scanner(System.in);
    
    public static int toInt(char c){
        if (c >= 'a' && c <= 'z') {
          return (int) c - (int)'a' + 1;  
        } else {
            return 0;   // Special characters do not count
        }
    }
    
    public static int calcValue(String str){
        int result = 0;
        for (char c : str.toLowerCase().toCharArray()) {
            result += toInt(c);
        }
        return result;
    }
    
    public static HashMap<String, Integer> evaluate (String input){
        // Add each word in string to the hashmap with its value
        // 1. Split into words
        // 2. Loop through each word and evaluate the value
        // 3. Sort and return
        HashMap<String,Integer> result = new HashMap<>();
        String[] split = input.split("\\s");
        
        for (String string : split) {
            if ("".equals(string)) {
                result.put(string, 0);
            } else {
                result.put(string, calcValue(string));
            }
        }
        
        return result;
    }
    
    public static void main(String[] args) {
        System.out.println("Enter Words to evaluate");
        String input = sc.nextLine();
        System.out.println(evaluate(input));
    }
}
