package com.challenge052.easy;

import java.util.HashMap;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 *
 * @author PR
 */
public class OrderAlphabetTest {
    
    public OrderAlphabetTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of toInt method, of class OrderAlphabet.
     */
    @Test
    public void testToInt() {
        System.out.println("toInt");
        char[] c = {'h','a', 't'};
        int[] expResult = {8,1,20};
        int[] result = new int[3];
        for (int i = 0; i < c.length ; i++) {
            result[i] =  OrderAlphabet.toInt(c[i]);
        }
        
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of calcValue method, of class OrderAlphabet.
     */
    @Test
    public void testCalcValue() {
        // Hat: 8+1+20 = 29
        // Shoe: 19+8+15+5 = 47
        System.out.println("calcValue");
        String[] str = {"Hat", "Shoe", ""};
        int[] expResult = {29,47,0};
        int[] result = new int[3];
        for (int i = 0; i < str.length; i++) {
            result[i] = OrderAlphabet.calcValue(str[i]);
        }
        
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of evaluate method, of class OrderAlphabet.
     */
    @Test
    public void testEvaluate() {
        System.out.println("evaluate");
        String input = "Hat Shoe";
        HashMap<String, Integer> expResult = new HashMap<>();
        expResult.put("Hat", 29);
        expResult.put("Shoe", 47);
        HashMap<String, Integer> result = OrderAlphabet.evaluate(input);
        assertEquals(expResult, result);
    }
    
}
