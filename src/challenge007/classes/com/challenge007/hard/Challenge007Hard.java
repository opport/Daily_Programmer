package com.challenge007.hard;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * This challenge will focus on creating a bot that can log into Reddit! Write a program that can log into a working
 * Reddit account. Since this challenge is vague, bonus points are awarded for a few different things: If the bot can
 * navigate the site and view posts If the bot can make posts If the bot can make statistics from the front page/any
 * subreddit. These statistics include time on front page, number of comments, upvotes, downvotes, etc. The more
 * functionality in your bot, the better.
 *
 * @author pr
 */
public class Challenge007Hard {

    private final static String URL = "www.reddit.com";

    private static void connect() {

        byte[] buffer = new byte[4096];

        try {
            URL url = new URL(URL);
            HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
            InputStream istream = urlc.getInputStream();

            urlc.setReadTimeout(10000); // 10s
            urlc.setDoOutput(true);

            istream.close();

        } catch (MalformedURLException e) {
            System.out.println(e);
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }

    }
}
