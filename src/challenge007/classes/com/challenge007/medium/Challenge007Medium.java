package com.challenge007.medium;

import javax.swing.JFrame;

/**
 * Write a program that draws a recursive image.
 *
 * For example, a Sierpinski triangle , a Barnsley fern, or a Mandelbrot set fractal
 *
 * would be good drawings.
 *
 * Any recursive image will do, but try to make them look fun or interesting.
 *
 * Bonus points for adding a color scheme!
 *
 * Please post a link to a sample image produced by your program, and above all, be creative.
 *
 * @author pr
 */
public class Challenge007Medium {

    public static void main(String[] args) {
        Fractals f = new Fractals();

        JFrame application = new JFrame();

        application.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        application.setSize(300, 300);
        application.setVisible(true);
        application.add(f);
    }
}
