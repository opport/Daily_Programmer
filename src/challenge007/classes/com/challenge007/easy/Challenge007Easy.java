package com.challenge007.easy;

import java.util.HashMap;

/**
 * Write a program that can translate Morse code in the format of ...---...
 *
 * A space and a slash will be placed between words. ..- / --.- For bonus, add the capability of going from a string to
 * Morse code. Super-bonus if your program can flash or beep the Morse. This is your Morse to translate:
 *
 * .... . .-.. .-.. --- / -.. .- .. .-.. -.-- / .--. .-. --- --. .-. .- -- -- . .-. / --. --- --- -.. / .-.. ..- -.-.
 * -.- / --- -. / - .... . / -.-. .... .- .-.. .-.. . -. --. . ... / - --- -.. .- -.--
 *
 * @author pr
 */
public class Challenge007Easy {

    /*
A 	.-
B 	-...
C 	-.-.
D 	-..
E 	.
F 	..-.
G 	--.
H 	....
I 	..
J 	.---
K 	-.-
L 	.-..
M 	--
Letter 	Morse
N 	-.
O 	---
P 	.--.
Q 	--.-
R 	.-.
S 	...
T 	-
U 	..-
V 	...-
W 	.--
X 	-..-
Y 	-.--
Z 	--..
Digit 	Morse
0 	-----
1 	.----
2 	..---
3 	...--
4 	....-
5 	.....
6 	-....
7 	--...
8 	---..
9 	----. */
    private final static HashMap<String, Character> ALPHABET = new HashMap<>() {
        {
            put(".-", 'A');
            put("-...", 'B');
            put("-.-.", 'C');
            put("-..", 'D');
            put(".", 'E');
            put("..-.", 'F');
            put("--.", 'G');
            put("....", 'H');
            put("..", 'I');
            put(".---", 'J');
            put("-.-", 'K');
            put(".-..", 'L');
            put("--", 'M');
            put("-.", 'N');
            put("---", 'O');
            put(".--.", 'P');
            put("--.-", 'Q');
            put(".-.", 'R');
            put("...", 'S');
            put("-", 'T');
            put("..-", 'U');
            put("...-", 'V');
            put(".--", 'W');
            put("-..-", 'X');
            put("-.--", 'Y');
            put("--..", 'Z');
            put("-----", '0');
            put(".----", '1');
            put("..---", '2');
            put("...--", '3');
            put("....-", '4');
            put(".....", '5');
            put("-....", '6');
            put("--...", '7');
            put("---..", '8');
            put("----.", '9');
        }
    };

    private final static String spaceToken = " / ";

    private final static String TO_TRANSLATE = ".... . .-.. .-.. --- / -.. .- .. .-.. -.-- / .--. .-. --- --. .-. .- -- -- . \n"
            + " .-. / --. --- --- -.. / .-.. ..- -.-. -.- / --- -. / - .... . / -.-. .... .- \n"
            + " .-.. .-.. . -. --. . ... / - --- -.. .- -.--";

    public static void main(String[] args) {
        String[] tokens = TO_TRANSLATE.split(spaceToken);

        StringBuilder sb = new StringBuilder();

        for (String t : tokens) {
            String[] split = t.split("\\s+");

            for (String s : split) {
                sb.append(ALPHABET.getOrDefault(s, ' '));
            }

            sb.append(" ");
            System.out.print(sb.toString());
            sb.setLength(0); // reset
        }
    }
}
