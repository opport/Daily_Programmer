/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.challenge232.medium;

import java.util.*;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 *
 * @author PR
 */
public class GrandmaHouseTest {
    
    public GrandmaHouseTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of generatePairs method, of class GrandmaHouse.
     */
    
    @Test
    public void testFindDuplicates() {
        System.out.println("findDuplicates");
        ArrayList<Coordinates> x = new ArrayList<>();
        x.add(new Coordinates(0.0,0.0));
        x.add(new Coordinates(2.0,2.0));
        ArrayList<Coordinates> y = new ArrayList<>();
        y.add(new Coordinates(0.0,0.0));
        y.add(new Coordinates(0.0,0.0));
        
        Coordinates expResultX = null;
        Coordinates expResultY = new Coordinates(0.0,0.0);
        
        Coordinates resultX = GrandmaHouse.findDuplicates(x);
        Coordinates resultY = GrandmaHouse.findDuplicates(y);
        assertEquals(expResultX, resultX);
    }
    /**
     * Test of calcDistance method, of class GrandmaHouse.
     */
    @Test
    public void testCalcDistance() {
        System.out.println("calcDistance");
        Coordinates a = new Coordinates(0.0, 0.0);
        Coordinates b = new Coordinates(0.0, 0.0);
        Coordinates c = new Coordinates(10.0, 10.0);
        double expResult = 0.0;
        double result = GrandmaHouse.calcDistance(a, c);
        assertNotSame(expResult, result);
    }

    /**
     * Test of findClosest method, of class GrandmaHouse.
     */
    @Test
    public void testFindClosest() {
        System.out.println("findClosest");
        ArrayList<Coordinates> x = new ArrayList<>();
        x.add(new Coordinates(10.0,10.0));
        x.add(new Coordinates(2.0,2.0));
        x.add(new Coordinates(0.0,0.0));
        ArrayList<Coordinates> y = new ArrayList<>();
        y.add(new Coordinates(0.0,0.0));
        y.add(new Coordinates(2.0,2.0));
        y.add(new Coordinates(0.0,0.0));
        
        ArrayList<Coordinates> expResultX = new ArrayList<>();
        expResultX.add(new Coordinates(10.0,10.0));
        expResultX.add(new Coordinates(2.0,2.0));
//        ArrayList<Coordinates> expResultY = new ArrayList<>();
//        expResultY.add(new Coordinates(0.0,0.0));
//        expResultY.add(new Coordinates(0.0,0.0));
        
        ArrayList<Coordinates> resultX = GrandmaHouse.findClosest(x);
        ArrayList<Coordinates> resultY = GrandmaHouse.findClosest(y);
        
        assertEquals(expResultX, resultX);
//        assertEquals(expResultY, resultY);
    }

    /**
     * Test of generatePairs method, of class GrandmaHouse.
     */
    @Test
    public void testGeneratePairs() {
        System.out.println("generatePairs");
        ArrayList<Coordinates> x = new ArrayList<>();
        ArrayList<Coordinates> y = new ArrayList<>();
        x.add(new Coordinates(10.0,10.0));
        x.add(new Coordinates(2.0,2.0));
        x.add(new Coordinates(0.0,0.0));
        
        
        ArrayList<Coordinates> expResult = null;
        ArrayList<Coordinates> expResult2 = new ArrayList<>();
        expResult2.add(new Coordinates(2.0, 2.0));
        expResult2.add(new Coordinates(0.0, 0.0));
        
        ArrayList<Coordinates> result = GrandmaHouse.generatePairs(x);
        ArrayList<Coordinates> result2 = GrandmaHouse.generatePairs(y);
        assertEquals(expResult2, result2);
    }

    /**
     * Test of main method, of class GrandmaHouse.
     */
    @Test
    public void testMain() {
        System.out.println("main");
        String[] args = null;
        GrandmaHouse.main(args);
    }
    
}
