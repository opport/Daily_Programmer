/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.challenge232.medium;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 *
 * @author PR
 */
public class CoordinatesTest {
    
    public CoordinatesTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of getX method, of class Coordinates.
     */
    @Test
    public void testGetX() {
        System.out.println("getX");
        Coordinates instance = new Coordinates(2.0, 2.0);
        Object expResult = 2.0;
        Object result = instance.getX();
        assertEquals(expResult, result);
    }

    /**
     * Test of getY method, of class Coordinates.
     */
    @Test
    public void testGetY() {
        System.out.println("getY");
        Coordinates instance = new Coordinates(2.0, 2.0);
        Object expResult = 2.0;
        Object result = instance.getY();
        assertEquals(expResult, result);
    }

    /**
     * Test of setX method, of class Coordinates.
     */
    @Test
    public void testSetX() {
        System.out.println("setX");
        Object x = 50;
        Coordinates instance = new Coordinates(0, 0);
        instance.setX((Comparable) x);
    }

    /**
     * Test of setY method, of class Coordinates.
     */
    @Test
    public void testSetY() {
        System.out.println("setY");
        Object y = 50;
        Coordinates instance = new Coordinates(0, 0);
        instance.setY((Comparable)y);
    }

    /**
     * Test of compareTo method, of class Coordinates.
     */
    @Test
    public void testCompareTo() {
        System.out.println("compareTo");
        Coordinates instance = new Coordinates(0, 0);
        int expResult = 0;
        int result = instance.compareTo(new Coordinates(20, 0));
        int result2 = instance.compareTo(new Coordinates(0, 0));
        assertNotSame(expResult, result);
        assertEquals(expResult, result2);
    }
    
    /**
     * Test of compareTo method, of class Coordinates.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Coordinates instance = new Coordinates(1.0,1.0);
        boolean expResult = true;
        boolean result = instance.equals(new Coordinates(1.0,1.0));
        assertEquals(expResult, result);
    }
    
}
