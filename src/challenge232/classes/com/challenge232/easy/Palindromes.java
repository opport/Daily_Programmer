package com.challenge232.easy;

public class Palindromes {

    public static boolean isPalindrome(String input) {

        // Remove all none-word characters (special characters) and remove capitalization
        String str = input.replaceAll("\\W", "").toLowerCase();
        int str_length = str.length();
        System.out.println(str);

        // Compare characters at both ends converging in the middle of the string
        for (int i = 0; i < (str.length() / 2); i++) {
            if (str.charAt(i) != str.charAt(str_length - i - 1)) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        String sample_input = "Was it a car\n"
                + "or a cat\n"
                + "I saw";

        System.out.println(isPalindrome(sample_input));
    }
}
