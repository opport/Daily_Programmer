package com.challenge232.medium;

/**
 * @param <X>
 * @param <Y>
 */
public class Coordinates<X extends Comparable<? super X>, Y extends Comparable<? super Y>> implements Comparable<Coordinates<X, Y>> {

    private X x;
    private Y y;

    public Coordinates(X x, Y y) {
        this.x = x;
        this.y = y;
    }

    public X getX() {
        return this.x;
    }

    public Y getY() {
        return this.y;
    }

    public void setX(X x) {
        this.x = x;
    }

    public void setY(Y y) {
        this.y = y;
    }

    @Override
    public int compareTo(Coordinates<X, Y> other) throws ClassCastException {
        if (!(other instanceof Coordinates)) {
            throw new ClassCastException("A Person object expected.");
        }

        return getX().compareTo(other.getX());
    }

    @Override
    public String toString() {
        return "(" + getX() + "," + getY() + ")";
    }

    public boolean equals(Coordinates<X, Y> other) throws ClassCastException {
        if (!(other instanceof Coordinates)) {
            throw new ClassCastException("A Person object expected.");
        }
        boolean eqX = this.getX().equals(other.getX());
        boolean eqY = this.getY().equals(other.getY());
        return eqX && eqY;
    }
}
