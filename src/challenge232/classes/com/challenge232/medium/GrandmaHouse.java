package com.challenge232.medium;

import java.util.*;

public class GrandmaHouse {

    static ArrayList<Coordinates> pairs = new ArrayList<>();
    static ArrayList<Coordinates> final_pair = new ArrayList<>(2);
    static double distance;

    public static double calcDistance(Coordinates a, Coordinates b) {
        Number x = Math.pow((double) b.getX() - (double) a.getX(), 2);
        Number y = Math.pow((double) b.getY() - (double) a.getY(), 2);
        return Math.sqrt(x.doubleValue() + y.doubleValue());
    }

    /**
     * Compare first element to others to find coord with the smallest distance
     * to it
     *
     * @param input
     * @return
     */
    public static ArrayList<Coordinates> findClosest(ArrayList<Coordinates> input) {
        double new_dist;

        if (input.size() == 1) {
            return null;
        }

        // Compare rest of values to initial ones to find smaller distances
        for (int i = 1; i < input.size(); i++) {
            // Calculate new distance to compare
            new_dist = calcDistance(input.get(0), input.get(i));

            // Replace old distance and coords with new ones if new_dist is shorter
            if (new_dist < distance) {
                final_pair.set(0, (Coordinates) input.get(0));
                final_pair.set(1, (Coordinates) input.get(i));
                distance = new_dist;
            }
        }
        // Remove shortest pair from comparison list
        input.remove(input.get(0));
        if (input.size() > 1) {
            findClosest(input);
        }

        return input;
    }

    public static Coordinates findDuplicates(ArrayList<Coordinates> pairs) {
        Coordinates duplicate = null;

        for (Coordinates t : pairs) {
            for (int i = pairs.indexOf(t) + 1; i < pairs.size(); i++) {

                // Comparison to find duplicate
                if (t.equals(pairs.get(i))) {
                    duplicate = t;
                    return duplicate;
                }
            }
        }
        return duplicate;
    }

    /**
     * Iterate through given coordinates to find pair with the smallest distance
     *
     * @return
     */
    public static ArrayList<Coordinates> generatePairs(ArrayList<Coordinates> input) {
        if (input == null || input.isEmpty()) {
            return input;
        }

        ArrayList<Coordinates> result = new ArrayList<>();
        result.add(findDuplicates(input));

        if (!result.isEmpty()) {
            result.add(result.get(0));
            return result;
        }

        distance = calcDistance(result.get(0), result.get(1));
        result = findClosest(input);

        return result;
    }

    public static void main(String[] args) {
        pairs.add(new Coordinates(0.0, 0.0));
        pairs.add(new Coordinates(10.0, 0.0));
        pairs.add(new Coordinates(0.0, 0.0));
        pairs.add(new Coordinates(2.0, 0.0));
//        pairs.add(new Coordinates(6.004788746281089, 7.071213090379764));
//        pairs.add(new Coordinates(8.104623252768594, 9.194871763484924));
//        pairs.add(new Coordinates(9.634479418727688, 4.005338324547684));
//        pairs.add(new Coordinates(6.743779037952768, 0.7913485528735764));
//        pairs.add(new Coordinates(5.560341970499806, 9.270388445393506));
//        pairs.add(new Coordinates(4.67281620242621, 8.459931892672067));
//        pairs.add(new Coordinates(0.30104230919622, 9.406899285442249));
//        pairs.add(new Coordinates(6.625930036636377, 6.084986606308885));
//        pairs.add(new Coordinates(9.03069534561186, 2.3737246966612515));
//        pairs.add(new Coordinates(9.3632392904531, 1.8014711293897012));
//        pairs.add(new Coordinates(2.6739636897837915, 1.6220708577223641));
//        pairs.add(new Coordinates(4.766674944433654, 1.9455404764480477));
//        pairs.add(new Coordinates(7.438388978141802, 6.053689746381798));

        //Collections.sort(pairs);
        pairs = generatePairs(pairs);
        System.out.println(pairs);
    }
}
