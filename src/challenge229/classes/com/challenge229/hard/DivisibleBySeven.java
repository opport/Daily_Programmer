package com.challenge229.hard;

import java.util.*;

public class DivisibleBySeven {

    public static List<Integer> divisible = new ArrayList<>();

    public static int exponent(int intLength) {
        if (intLength < 2) {
            return 1;
        } else {
            return (10 * exponent(intLength - 1));
        }
    }

    public static int intLength(int x) {
        if (x < 1) {
            return 0;
        } else {
            return (1 + intLength(x / 10));
        }
    }

    public static int reverse(int number) {
        int result = 0;

        while (number > 0) {
            result += (number % 10) * exponent(intLength(number));
            number -= number % 10;
            number /= 10;
        }

        return result;
    }

    public static int findNumbers(int limit) {
        int sum = 0;

        //Iterate until the limit has been reached
        for (int i = 7; i <= limit; i += 7) {
            // Check if both normal and reversed number are divisible
            if ((i % 7 == 0) && reverse(i) % 7 == 0) {

                // Add normal number if it hasn't been added
                if (!divisible.contains(i)) {
                    sum += i;
                    divisible.add(i);

                    // Add reverse if it does not equal the number
                    if (i != reverse(i) && !divisible.contains(i)) {
                        sum += reverse(i);
                        divisible.add(reverse(i));
                    }
                }
            }
        }
        Collections.sort(divisible);
        return sum;
    }

    public static void main(String[] args) {
        int limit = 1000;
        System.out.println("Sum of divisibleBySeven until " + limit + "= " + findNumbers(limit));
        System.out.println(divisible);
        //System.out.println(reverse(234));
        //System.out.println(intLength(234));
        //System.out.println(exponent(intLength(234)));

    }
}
