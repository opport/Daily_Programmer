package algorithmsCourse.lesson1;

import java.util.*;

public class Lesson1 {

    /**
     * 1. Schreiben Sie zwei C-Funktionen min() und max(), die aus zwei
     * gegebenen Integerzahlen das Minimum bzw. das Maximum bestimmen!
     */
    public static int min(int a, int b) {
        if (a < b) {
            return a;
        } else {
            return b;
        }
    }

    public static int max(int a, int b) {
        if (a > b) {
            return a;
        } else {
            return b;
        }
    }

    /**
     * 2.Schreiben Sie eine Funktion sum(), die in Abhängigkeit von einem
     * gegebenen Funktionsparameter N alle natürlichen Zahlen von 1 bis N
     * addiert. Wie lautet die Vorbedingung des Algorithmus? Sichern Sie die
     * Einhaltung der Vorbedingung zu. Formulieren Sie zwei Varianten des
     * Algorithmus: a)sum() berechnet das Funktionsergebnis mittels Iteration
     *
     * b)sum() berechnet das Funktionsergebnis ohne Iteration
     */
    public static int sumIterativ(int n) {

        if (n < 1) {
            return 0;
        }

        int summe = 0;
        for (int i = 1; i <= n; i++) {
            summe += i;
        }

        return summe;
    }

    public static int sumRekursiv(int n) {

        if (n < 1) {
            return 0;
        }

        return n + sumRekursiv(n - 1);
    }

    /**
     * 3.Schreiben Sie eine C-Funktion mul(), die zwei positive Integerzahlen
     * miteinander multipliziert. Realisieren Sie die Multiplikation
     * ausschließlich mit Hilfe der Addition! Wie lautet die Vorbedingung Ihres
     * Algorithmus? Sichern Sie die Einhaltung der Vorbedingung zu!
     */
    public static int mul(int n, int m) {
        if (n < 1 || m < 1) {
            return 0;
        }
        return n + mul(n, (m - 1));
    }

    /**
     * 4.Erweitern Sie die Funktion aus 3. so dass nun beliebige ganze Zahlen
     * miteinander multipliziert werden können. Was müssen Sie dabei beachten?
     */
    public static int mul2(int n, int m) {
        if (n == 0 || m == 0) {
            return 0;
        } else if ((n < 0) ^ (m < 0)) {
            if (n < 0) {
                return n - mul2(n, (m - 1));
            } else {
                return m - mul2((n - 1), m);
            }
        } else {
            return n + mul2(n, (m - 1));
        }
    }

    /**
     * 5.Schreiben Sie ein Programm, das auf dem Bildschirm den Begriff
     * „TU-Chemnitz“ ungefähr in folgender Form ausgibt: Geben Sie Ihren Namen
     * auch in dieser Form aus! **** * * **** * * ***** * * * * ***** *****
     * ***** * * * * * * * ** ** ** * * * * * * * ***** * ***** **** * * * * * *
     * * * * * * * * * * * * * * ** * * * * *** **** * * ***** * * * * ***** *
     * *****
     */
    public static void sterne() {
        List<List<String>> group = new ArrayList<>(5);
    }

    /**
     * Ein Mähroboter soll ein 200m x 100m große Wiese mähen. Aufgrund seiner
     * Konstruktion kann er sich entweder nur fortbewegen oder die Position auf
     * der er steht, mähen. Als Grundoperationen stehen folgende Aktionen zur
     * Verfügung. • mow - Mähen der Position auf der der Roboter steht • turn -
     * Rechtsdrehung (im Uhrzeigersinn) um 90 Grad • move - Bewegung um 1m in
     * eingestellter Richtung Des Weiteren besitzt der Roboter zwei
     * Zahlenspeicher a und b für ganze Zahlen, denen beliebige Werte zugewiesen
     * und mit denen die Grundrechenarten sowie Vergleiche durchgeführt werden
     * können. Formulieren Sie einen Algorithmus, der den Roboter die Wiese
     * komplett abmähen und anschließend zu seiner Ausgangsposition zurückkehren
     * lässt! Benutzen Sie dazu die beschriebenen Grundoperationen und die
     * kennengelernten Steuerstrukturen und verwenden Sie weitestgehend
     * CSchreibweise. Beachten Sie, dass der Roboter den zu mähenden Bereich
     * nicht verlassen darf! Welche Vorbedingungen müssen gelten?
     */
    public static void maehRobotor() {
        final int[] WIESE = {200, 100};
        final char[] RICHTUNGEN = {'n', 'w', 's', 'o'};
        char richtung = 'n';
        int[] anfang = {0, 0};
        int a, b;
        a = b = 0;
    }

    /**
     * Ein Geldautomat soll den Betrag X (ein beliebiges Vielfaches von 5€)
     * auszahlen. Es stehen dafür gewöhnliche Banknoten im Wert von 5€ bis 500€
     * zur Verfügung. Beschreiben Sie einen Algorithmus, der eine Stückelung der
     * Banknoten berechnet, die dem Betrag X entspricht. Die Stückelung soll
     * durch so wenig Banknoten wie möglich realisiert werden. a)Der Vorrat an
     * Geldscheinen ist für jede Sorte hinreichend groß b)Der Vorrat an
     * Geldscheinen ist begrenzt. Die Zahlen N5€, ..., N500€ geben den Vorrat
     * für jede Sorte an.      *
     * Bedenken Sie, dass ein Betrag nicht auszahlbar sein kann, obwohl genug
     * Geld vorrätig ist (z.B. können 15€ nicht ausgezahlt werden, wenn es keine
     * 5€-Scheine mehr gibt).
     *
     * @author PR
     * @param betrag
     */
    public static void geldAutomat(int betrag) {
        int schein500, schein200, schein100, schein50, schein20, schein10, schein5;
        schein500 = schein200 = schein100 = schein50 = schein20 = schein10 = schein5 = 0;

        if (betrag % 5 != 0) {
            System.out.println("Eingabe nicht durch 5 Teilbar.");
            return;
        }

        System.out.println("Scheinausgabe für " + betrag + "€");
        while (betrag > 0) {
            // Anzahl fuer jeden Schleifendurchgang erneut auf 0 setzen
            int anzahl = 0;

            if (betrag >= 500) {
                anzahl += betrag / 500;
                betrag -= anzahl * 500;
                System.out.println("500: \t" + anzahl);
            } else if (betrag >= 200) {
                anzahl += betrag / 200;
                betrag -= anzahl * 200;
                System.out.println("200: \t" + anzahl);
            } else if (betrag >= 100) {
                anzahl += betrag / 100;
                betrag -= anzahl * 100;
                System.out.println("100: \t" + anzahl);
            } else if (betrag >= 50) {
                anzahl += betrag / 50;
                betrag -= anzahl * 50;
                System.out.println("50: \t" + anzahl);
            } else if (betrag >= 20) {
                anzahl += betrag / 20;
                betrag -= anzahl * 20;
                System.out.println("20: \t" + anzahl);
            } else if (betrag >= 10) {
                anzahl += betrag / 10;
                betrag -= anzahl * 10;
                System.out.println("10: \t" + anzahl);
            } else if (betrag >= 5) {
                anzahl += betrag / 5;
                betrag -= anzahl * 5;
                System.out.println("5: \t" + anzahl);
            }
        }
    }

    public static void main(String[] args) {
        geldAutomat(195);
    }

}
