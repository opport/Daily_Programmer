package com.challenge006.hard;

/**
 * create a AI that will play NIM
 *
 * The normal game is between two players and played with three heaps of any number of objects. The two players
 * alternate taking any number of objects from any single one of the heaps. The goal is to be the last to take an
 * object. In misère play, the goal is instead to ensure that the opponent is forced to take the last remaining object.
 *
 * The following example of a normal game is played between fictional players Bob and Alice who start with heaps of
 * three, four and five objects.
 *
 * Sizes of heaps Moves A B C
 *
 * 3 4 5 Bob takes 2 from A 1 4 5 Alice takes 3 from C 1 4 2 Bob takes 1 from B 1 3 2 Alice takes 1 from B 1 2 2 Bob
 * takes entire A heap, leaving two 2s. 0 2 2 Alice takes 1 from B 0 1 2 Bob takes 1 from C leaving two 1s. (In misère
 * play he would take 2 from C leaving (0, 1, 0).) 0 1 1 Alice takes 1 from B 0 0 1 Bob takes entire C heap and wins.
 *
 *
 * @author pr
 */
public class Challenge006Hard {
    //TODO
}
