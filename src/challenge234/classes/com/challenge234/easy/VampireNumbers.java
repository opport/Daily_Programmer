package com.challenge234.easy;

import java.util.*;
import java.util.Scanner;

public class VampireNumbers {

    public static int exponentiate(int intLength) {
        if (intLength < 2) {
            return 1;
        } else {
            return (10 * exponentiate(intLength - 1));
        }
    }

    public static int intLength(int x) {
        if (x < 1) {
            return 0;
        } else {
            return (1 + intLength(x / 10));
        }
    }

    public static int containsChar(char[] ch, char c) {
        for (int i = 0; i < ch.length; i++) {
            if (ch[i] == c) {
                return i;
            }
        }
        return -1;
    }

    public static boolean compare(Integer a, Integer b) {
        char[] chA = a.toString().toCharArray();
        char[] chB = b.toString().toCharArray();
        char[] chP = ((Integer) (a * b)).toString().toCharArray();

        // Check if sum of factor digits equals product digits
        if (chA.length + chB.length != chP.length) {
            return false;
        }

        // Check for digits in factor A
        for (int i = 0; i < chA.length; i++) {
            if (containsChar(chP, chA[i]) == -1) {
                return false;
            } else {
                chP[containsChar(chP, chA[i])] = 'c';
            }
        }

        // Check for digits in factor B        
        for (int i = 0; i < chB.length; i++) {
            if (containsChar(chP, chB[i]) == -1) {
                return false;
            } else {
                chP[containsChar(chP, chB[i])] = 'c';
            }
        }
        return true;
    }

    public static List<Triple> findNumbers(int length) {
        List<Triple> result = new ArrayList<>();

        for (Integer i = 1; intLength(i) < length; i++) {
            // Ensure that number does not have two trailing zeros
            if (i > 99 && i % 10 == 0 && ((i / 10) % 10) == 0) {
                i++;
            }

            // Ensure that sum of i+j lengths does not exceed set limit
            for (Integer j = i; intLength(i) + intLength(j) <= length; j++) {
                // Ensure that number does not end in zero
                if (i > 99 && i % 10 == 0 && ((i / 10) % 10) == 0) {
                    j++;
                }

                if (compare(i, j) == true) {
                    result.add(new Triple(i, j, (i * j)));
                }
            }
        }

        result.forEach((temp) -> {
            temp.print();
        });

        return result;
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Number of digits for vampire numbers: ");
        int length = input.nextInt();

        findNumbers(length);
    }
}
