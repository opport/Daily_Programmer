/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.challenge234.easy;

/**
 *
 * @author PR
 */
public class Triple<A, B, C> {

    private A a;
    private B b;
    private C c;

    public Triple(A a, B b, C c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public A getA() {
        return a;
    }

    public B getB() {
        return b;
    }

    public C getC() {
        return c;
    }

    public void setA(A a) {
        this.a = a;
    }

    public void setB(B b) {
        this.b = b;
    }

    public void setC(C c) {
        this.c = c;
    }

    public void print() {
        System.out.println(c + "=" + a + "*" + b);
    }

//    public int compare(Triple t1, Triple t2) {
//
//	   A a1 = t1.getA();
//	   A a2 = t2.getA();
//
//	   /*For ascending order*/
//	   return rollno1-rollno2;
//
//	   /*For descending order*/
//	   //rollno2-rollno1;
//   }};
//    
}
