package com.challenge242.easy;

public class Plant {

    private int age;

    public Plant() {
        age = 0;
    }

    public void incrementAge() {
        age++;
    }

    public int getAge() {
        return age;
    }
}
