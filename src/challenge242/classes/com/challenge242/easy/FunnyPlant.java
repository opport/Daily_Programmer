package com.challenge242.easy;

import java.util.*;

public class FunnyPlant {

    private static ArrayList<Plant> plants = new ArrayList<>();
    static Scanner sc = new Scanner(System.in);
    static int weeks = 0;

    public static void calculate(int people, int plant_number) {
        if (people < 1) {
            return;
        }

        // Add Initial plants
        for (int i = 0; i < plant_number; i++) {
            plants.add(new Plant());
        }
        // Variable fruits to keep track of number of fruits harversted in week
        int total = 0;

        while (total < people) {
            weeks++;
            total = 0;

            for (int i = 0; i < plants.size(); i++) {
                // Add up total amount of harvestable fruit
                total += plants.get(i).getAge();

                // Add new plant for each week past 0
                for (int j = 0; j < plants.get(i).getAge(); j++) {
                    plants.add(new Plant());
                }

                //System.out.println("weeks: " + weeks + ", plants: " + plants.size() + ", fruits: " + total + ", plant age: " + plants.get(i).getAge());
                //Increment age
                plants.get(i).incrementAge();
            }

        }
    }

    public static void main(String[] args) {
        int people, plant_number;

        System.out.println("How many people to feed? ");
        people = sc.nextInt();
        System.out.println("How many inital plants? ");
        plant_number = sc.nextInt();

        calculate(people, plant_number);
        System.out.println("People: " + people + ", Plants: " + plant_number + ", Weeks:" + weeks);
    }
}
