package com.challenge051.hard;

import com.challenge221.easy.Direction;
import java.util.Random;

public class DominoBoard {
    public static final Random RANDOM = new Random();   // Get random placement for current domino
    public static final Direction[][] BOARD = new Direction[7][7];
    
    
    public static void addHorizontal(int x, int y){
        BOARD[x][y] = Direction.RIGHT;
        BOARD[x][y+1] = Direction.LEFT;
    }
    
    public static void addVertical(int x, int y){
        BOARD[x][y] = Direction.DOWN;
        BOARD[x+1][y] = Direction.UP;
    }
    
    public static final void makeBoard(){
        for (int i = 0; i < BOARD.length; i++) {
            for (int j = 0; j < BOARD.length; j++) {
                if (BOARD[i][j] != null || (i == 3 && j == 3)) {    // Piece already set
                    continue;                                       // or [3][3] reached
                }
                
                if (RANDOM.nextInt(100) < 50) {
                    if (j+1 < BOARD.length              // Check for horizontal bound, 
                            && BOARD[i][j+1] == null    // next char in line
                            && (i != 3 && j + 1 != 3)) {  // and [3][3]
                        addHorizontal(i, j);    // Lay horizontally
                    } else {                    // Lay vertically
                        addVertical(i, j);
                    }
                } else {
                    if (i+1 < BOARD.length              // Check for vertical bound, 
                            && BOARD[i+1][j] == null    // next char beneath
                            && (i+1 != 3 && j != 3)) {  // and [3][3]
                        addVertical(i, j);      // Lay vertically
                    } else {                    
                        addHorizontal(i, j);    // Lay horizontally
                    }
                }
            }
        }
    }
    
    public static void main(String[] args) {
        makeBoard();
        for (int i = 0; i < BOARD.length; i++) {
            for (int j = 0; j < BOARD.length; j++) {
                System.out.print(" " + BOARD[i][j]);
            }
            System.out.println("");
        }
    }
}
