package parseCoordinates;

import challenge232.intermediate.grandmaHouse.*;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ParseCoordinates {
    private static Path fPath = null;
    private static int totalDistance = 0;
    private static Scanner sc;   
    private static Coordinates point = null;
    
    public static void parseLine(String line){
        int x;
        int y;
        
        Scanner s = new Scanner(line);
        // Separate at any non alphanumeric character
        s.useDelimiter("\\W");
        if(s.hasNext()){
            if("go".equals(s.next())){
                x = s.nextInt();
                y = s.nextInt();
                Coordinates c = new Coordinates(x,y);
                if (point != null) {
                    totalDistance += GrandmaHouse.calcDistance(point,c);
                    point = c;
                } else {
                    point = c;
                }
            }
        }
    }
    
    public static final void parseFile(Path path) throws IOException{
        sc = new Scanner(path);
        while (sc.hasNextLine()) {
            parseLine(sc.nextLine());
        }
    }
    
    public static void main(String[] args) throws IOException{
            fPath = Paths.get(args[1]);
            parseFile(fPath);
    }
}
