package com.challenge008.medium;

import java.util.HashMap;
import java.util.Scanner;

/**
 * Write a program that will print the english name of a value. for example, "1211" would become "one-thousand, two
 * hundred, eleven".
 *
 * for extra credit, allow it to read the english value of a number and output the integer.
 *
 * input: one-hundred, four output: 104
 *
 * @author pr
 */
public class Challenge008Medium {

    private static final HashMap<Integer, String> noughts = new HashMap<Integer, String>() {
        {
            put(0, "zero");
            put(1, "one");
            put(2, "two");
            put(3, "three");
            put(4, "four");
            put(5, "five");
            put(6, "six");
            put(7, "seven");
            put(8, "eight");
            put(9, "nine");
        }
    };

    private static final HashMap<Integer, String> teens = new HashMap<Integer, String>() {
        {
            put(11, "eleven");
            put(12, "twelve");
            put(13, "thrirteen");
            put(14, "fourteen");
            put(15, "fifteen");
            put(16, "sixteen");
            put(17, "seventeen");
            put(18, "eighteen");
            put(19, "nineteen");
        }
    };

    private static final HashMap<Integer, String> tens = new HashMap<Integer, String>() {
        {
            put(10, "ten");
            put(20, "twenty");
            put(30, "thrirty");
            put(40, "forty");
            put(50, "fifty");
            put(60, "sixty");
            put(70, "seventy");
            put(80, "eighty");
            put(90, "ninety");
        }
    };

    private static final HashMap<Integer, String> powersOfTen = new HashMap<Integer, String>() {
        {
            put(3, "hundred");
            put(4, "thousand");
            put(5, "thousand");
            put(6, "hundred thousand");
            put(7, "million");
            put(8, "million");
            put(9, "hundred million");
            put(10, "billion");
            put(11, "billion");
            put(12, "hundred billion");
            put(13, "trillion");
            put(14, "trillion");
            put(15, "hundred trillion");
        }
    };

    private static void transcribeNoughts(Integer number) {
        System.out.print(noughts.get(number));
    }

    private static void transcribeTeens(Integer number) {
        System.out.print(teens.get(number));
    }

    private static void transcribeMultiplesOfTen(Integer number) {
        System.out.print(tens.get(number - (number % 10)));
    }

    private static int printPowerOfTen(int number, int length) {

        int digitReduction = 1;

        // check (thousand|million|billion|trillion) for possible tens prefix
        if (((length - 5) % 3) == 0) {
            int prefix = number / (int) (Math.pow(10, length - 2));

            if (prefix < 10) {
                transcribeNoughts(prefix);
            } else if (prefix > 10 && prefix < 20) {
                transcribeTeens(prefix);
                digitReduction = 2; // make sure only one digit prefix is printed 
            }
            System.out.print(" ");
        } else {
            int prefix = number / (int) (Math.pow(10, length - 1));
            transcribeNoughts(prefix);
            System.out.print(" ");
        }

        System.out.print(powersOfTen.get(length));
        System.out.print(" ");

        number = number % (int) Math.pow(10, length - digitReduction);
        return number;
    }

    private static void transcribePowersOfTen(Integer number) {
        while (number.toString().length() > 2) {
            int length = number.toString().length();

            if (powersOfTen.containsKey(length)) {
                number = printPowerOfTen(number, length);
            } else { // take care of prefixes
                number = number % (int) Math.pow(10, length - 1);
            }
        }

        if (number > 0) {
            System.out.print(" and ");
            if (number < 10) {
                transcribeNoughts(number);
            } else if (number > 10 && number < 20) {
                transcribeTeens(number);
            } else if (number < 100) {
                transcribeMultiplesOfTen(number);
                int next = number % 10;

                if (next != 0) {
                    transcribeNoughts(next);
                }
            }
        }
    }

    private static void transcribe(Integer number) {

        // Add sign if necessary
        if (number < 0) {
            System.out.print("Minus ");
            number = -number;
        }

        if (number < 10) {
            transcribeNoughts(number);
        } else if (number > 10 && number < 20) {
            transcribeTeens(number);
        } else if (number < 100) {
            transcribeMultiplesOfTen(number);
            int next = number % 10;

            if (next != 0) {
                transcribeNoughts(next);
            }
        } else if (number.toString().length() < 14) {
            transcribePowersOfTen(number);
        }

        System.out.println("");
    }

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.print("Enter number to transcribe: ");

//        Integer number;
//
//        while (true) {
//            try {
//                number = s.nextInt();
//                s.nextLine(); // flush enter
//                break;
//            } catch (Exception e) {
//                System.out.println("Please enter a valid number");
//            }
//        }
//
//        transcribe(number);
        transcribe(1011110);
    }
}
