package com.challenge008.easy;

/**
 * write a program that will print the song "99 bottles of beer on the wall".
 *
 * for extra credit, do not allow the program to print each loop on a new line.
 *
 * @author pr
 */
public class Challenge008Easy {

    private static final String LASTVERSE = "No more bottles of beer on the wall, no more bottles of beer.\n"
            + "Go to the store and buy some more, 99 bottles of beer on the wall.";

    public static void main(String[] args) {
        int number = 99;
        while (number > 0) {
            System.out.println(String.format("%s bottles of beer on the wall, %s bottles of beer.\nTake one down and pass it around, %s bottles of beer on the wall.", number, number, number - 1));
            number--;
        }

        System.out.println(LASTVERSE);
    }
}
