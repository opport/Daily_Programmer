package com.challenge008.hard;

import java.util.Scanner;

/**
 * Write a program that will take coordinates, and tell you the corresponding number in pascals triangle. For example:
 *
 * Input: 1, 1 output:1
 * <br>
 * input: 4, 2 output: 3
 * <br>
 * input: 1, 19 output: error/nonexistent/whatever
 * <br>
 * the format should be "line number, integer number" for extra credit, add a function to simply print the triangle, for
 * the extra credit to count, it must print at least 15 lines.
 *
 * @author pr
 */
public class Challenge008Hard {

    private static final String ERROR = "Input does not compute";

    private static int fac(int i) {
        int result = 1;
        while (i > 0) {
            result *= i;
            i--;
        }

        return result;
    }

    private static String getPascalResult(int n, int k) {
        n--;
        k--;

        if (n < 0 || k < 0) {
            return ERROR;
        }

        int nominator = fac(n);
        int denominator = fac(k) * fac(n - k);

        int result = nominator / denominator;
//        int result = (fac(n)) / (fac(k) * fac(n - k));

        if (result == 0) {
            return ERROR;
        }
        return "output: " + result;
    }

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);

//        System.out.println(getPascalResult(1, 1));
//        System.out.println(getPascalResult(4, 2));
//        System.out.println(getPascalResult(1, 19));
//        System.out.println(getPascalResult(6, 2));
//        System.out.println(getPascalResult(6, 5));
        System.out.println("Enter coordinates for pascal's triangle");
        while (true) {
            System.out.print("x: ");
            int x = s.nextInt();
            s.nextLine(); // flush enter
            System.out.print("\ty: ");
            int y = s.nextInt();
            s.nextLine(); // flush enter        

            System.out.println(getPascalResult(x, y));
        }
    }
}
