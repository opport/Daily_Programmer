/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.challenge243.medium;

/**
 *
 * @author PR
 */
public class Triple implements Comparable<Triple> {

    private String key;
    private int value;
    private int quantity = 0;

    public Triple() {
    }

    public Triple(String key, Integer value) {
        this.key = key;
        this.value = value;
    }

    public Triple(String key, Integer value, Integer quantity) {
        this.key = key;
        this.value = value;
        this.quantity = quantity;
    }

    public String getKey() {
        return this.key;
    }

    public int getValue() {
        return this.value;
    }

    public int getQuantity() {
        return this.quantity;
    }

    public void incrementQuantity() {
        this.quantity++;
    }

    @Override
    public int compareTo(Triple other) throws ClassCastException {
        if (!(other instanceof Triple)) {
            throw new ClassCastException("A Person object expected.");
        }
        String otherKey = other.getKey();
        return this.key.compareTo(otherKey);
    }

    @Override
    public String toString() {
        return getKey() + "," + getValue() + "," + getQuantity();
    }

}
