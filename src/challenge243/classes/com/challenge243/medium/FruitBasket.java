package com.challenge243.medium;

import java.util.*;

public class FruitBasket {

    private static final ArrayList<ArrayList<Triple>> solutions = new ArrayList<>();
    private static HashMap<String, Integer> input_options = new HashMap<>();
    private static final ArrayList<Triple> optionsTriple = new ArrayList<>();
    private final static int PENNIES = 500;

    public static void initializeTriples() {
        for (Map.Entry<String, Integer> item : input_options.entrySet()) {
            optionsTriple.add(new Triple(item.getKey(), item.getValue(), 0));
        }
    }

    public static void findSolutions(int remainder) {
        // Find out if successful solution was found
        if (remainder == 0) {
            //solutions.add(optionsTriple);
            solutions.add(optionsTriple);
            optionsTriple.clear();
        } else {
            // Find out whether function called another one
            int calls = 0;

            // Iterate through values
            Iterator entries = input_options.entrySet().iterator();

            while (entries.hasNext()) {
                HashMap.Entry entry = (HashMap.Entry) entries.next();
                String key = (String) entry.getKey();
                Integer value = (Integer) entry.getValue();

                // Branch possible
                if (remainder - value >= 0) {
                    optionsTriple.get(optionsTriple.indexOf(key)).incrementQuantity();
                    findSolutions(remainder - value); // Recur until no option possible
                    return;
                }
            }

            // Clear list if no calls were made --> dead end branch
            if (calls == 0) {
                input_options.clear();
            }
        }

    }

    public static void main(String[] args) {
        HashMap<String, Integer> sample_input = new HashMap<>();
        sample_input.put("banana", 32);
        sample_input.put("kiwi", 41);
        sample_input.put("mango", 97);
        sample_input.put("papaya", 254);
        sample_input.put("pineapple", 399);

        input_options = sample_input;
        findSolutions(PENNIES);
        System.out.println(solutions);
    }
}
