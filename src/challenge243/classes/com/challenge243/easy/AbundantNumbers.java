package com.challenge243.easy;

/**
 * In number theory, a deficient or deficient number is a number n for which the
 * sum of divisors sigma(n)<2n, or, equivalently, the sum of proper divisors (or
 * aliquot sum) s(n)<n. The value 2n - sigma(n) (or n - s(n)) is called the
 * number's deficiency. In contrast, an abundant number or excessive number is a
 * number for which the sum of its proper divisors is greater than the number
 * itself @author pr
 */
public class AbundantNumbers {

    /**
     * Calculate deficiency for normal divisors.
     *
     * @param number Input number to be evaluated for deficiency.
     * @return Level of deficiency with 0 notating abundance.
     */
    static private int divisors(int number) {

        if (number < 1) {
            return 0;
        }

        int sum_div = 0;

        // Loop to find all divisors including number itself and add them to the resulting sum
        for (int i = 1; i <= number; i++) {
            if (number % i == 0) {
                sum_div += i;
            }
        }

        // Return deficiency or abundance
        return sum_div - (2 * number);
    }

    /**
     * Calculate deficiency for proper divisors.
     *
     * @param number Input number to be evaluated for deficiency.
     * @return Level of deficiency with 0 notating abundance.
     */
    static private int divisorsProper(int number) {
        if (number < 1) {
            return 0;
        }

        int sum_div = 0;

        // Loop to find divisors and add them to the resulting sum
        for (int i = 1; i < number; i++) {
            if (number % i == 0) {
                sum_div += i;
            }
        }

        // Return deficiency or abundance
        return sum_div - number;
    }

    static private boolean isDeficient(int number) {

        int result = Integer.min(divisors(number), divisorsProper(number));

        if (result > 0) {
            System.out.println(number + " is abundant by \t" + result);
            return true;
        } else {
            System.out.println(number + " is deficient by \t" + -result);
            return false;
        }
    }

    public static void main(String[] args) {
        int[] sample_input = {111, 112, 220, 69, 134, 85};

        for (int i : sample_input) {
            System.out.println(isDeficient(i));
        }

    }
;
}
