package com.challenge243.mini;

import java.util.Random;
import java.util.Scanner;

/**
 * To base n - Convert a base 10 number to given base
 *
 * Given: an integer x in base 10 and an integer base n between 2 and 10
 *
 * Output: x in base n Example input: 987 4 Example output: 33123
 *
 * Extra: Make it work for bases greater than 10 (using letters) Challenge
 * input: 1001 8
 *
 * @author PR
 */
public class ToBaseN {

    static int exponent = 0;

    public static int exponentiate(int intLength) {
        if (intLength < 2) {
            return 1;
        } else {
            return (10 * exponentiate(intLength - 1));
        }
    }

    public static int convert(int number, int base) {
        exponent += 1;

        if (number < 2) {
            System.out.println(exponent);
            return number * exponentiate(exponent);
        } else {
            return (number % base) * exponentiate(exponent) + convert(number / base, base);
        }
    }

    public static void main(String[] args) {
        // Random number to start the challenge
        Random random = new Random();
        //Input number
        Scanner input = new Scanner(System.in);
        System.out.print("Decimal number: ");
        int number = input.nextInt();
        System.out.print("To base: ");
        int base = input.nextInt();

        System.out.println(number + " to base " + base + " = " + convert(number, base));
    }
}
