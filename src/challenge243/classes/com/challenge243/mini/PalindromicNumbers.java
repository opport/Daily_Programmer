package com.challenge243.mini;

import com.challenge229.hard.DivisibleBySeven;
import com.challenge232.easy.Palindromes;
import java.util.*;

/**
 * Palindromic numbers - Output palindromic numbers. Given: OPTIONAL input: the minimum number of digits to consider
 * Output: A list of palindromic numbers. You can decide how (and if) to end the sequence. Bonus: Let the user decide
 * what base to use.
 *
 * @author PR
 */
public class PalindromicNumbers {

    public static List<Integer> palindromes(int start, int end) {
        List<Integer> result = new ArrayList<>();

        for (int i = start; i <= end; i++) {
            int j = i;
            while (!Palindromes.isPalindrome(String.valueOf(j))) {
                j += DivisibleBySeven.reverse(j);
            }
            result.add(j);
        }

        return result;
    }

    public static void main(String[] args) {
        // Random number to start the challenge
        //Input number
        Scanner input = new Scanner(System.in);
        System.out.print("Palindromize numbers from ");
        int start = input.nextInt();
        System.out.print("until ");
        int end = input.nextInt();

        System.out.println(palindromes(start, end));
    }
}
