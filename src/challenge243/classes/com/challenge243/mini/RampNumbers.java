package com.challenge243.mini;

import java.util.Random;

/**
 * Ramp Numbers - A ramp number is a number whose digits from left to right
 * either only rise or stay the same. 1234 is a ramp number as is 1124. 1032 is
 * not. Given: A positive integer, n.
 *
 * Output: The number of ramp numbers less than n.
 *
 * Example input: 123 Example output: 65
 *
 * Challenge input: 99999 Challenge output:
 *
 * @author PR
 */
public class RampNumbers {

    public static boolean evaluate(int number) {
        if (number == 0) {
            return true;
        } else if ((number % 10) >= (number / 10) % 10) {
            return evaluate(number / 10);
        } else {
            return false;
        }
    }

    public static void main(String[] args) {
        // Random number to start the challenge
        Random random = new Random();
        int number = random.nextInt(1000);

        System.out.print(number + "=" + evaluate(number));
    }
}
