package com.challenge245.easy;

public class DateDilemma {

    public static int toInt(char ch) {
        return (int) ch - (int) '0';
    }

    public static String[] normalize(String[] new_input) {
        new_input[0] = normalizeYear(new_input);
        new_input[1] = normalizeMonth(new_input);
        new_input[2] = normalizeDay(new_input);
        return new_input;
    }

    public static String normalizeYear(String[] new_input) {
        if (new_input[0].length() == 2) {
            if (toInt(new_input[0].charAt(0)) > 1) {
                return "19" + new_input[0]; // year was short for 20th century
            } else {
                return "20" + new_input[0];
            }
        } else {
            return new_input[0];
        }
    }

    public static String normalizeMonth(String[] new_input) {
        if (new_input[1].length() == 1) {
            return "0" + new_input[1];
        } else {
            return new_input[1];
        }
    }

    public static String normalizeDay(String[] new_input) {
        if (new_input[2].length() == 1) {
            return "0" + new_input[2];
        } else {
            return new_input[2];
        }
    }

    public static void parseInput(String[] input) {
        String[] new_input = new String[3];
        String result = new String();

        for (String str : input) {
            // Remove special characters
            String[] numbers = str.split("\\D");

            // Determine what date format has been entered
            if (numbers[0].length() == 4) {
                System.arraycopy(numbers, 0, new_input, 0, 3);
                result = String.join("-", new_input);

                // final normalization
                new_input = normalize(new_input);
            } else {
                new_input[0] = numbers[2];
                new_input[1] = numbers[0];
                new_input[2] = numbers[1];

                // final normalization
                new_input = normalize(new_input);

                result = String.join("-", new_input);
            }

            System.out.println(result);

        }
    }

    public static void main(String[] args) {
        String[] sample_input = new String[]{"2/13/15",
            "1-31-10",
            "5 10 2015",
            "2012 3 17",
            "2001-01-01",
            "2008/01/07"
        };
        parseInput(sample_input);
    }
}
