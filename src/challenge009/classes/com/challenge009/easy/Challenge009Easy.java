package com.challenge009.easy;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * write a program that will allow the user to input digits, and arrange them in numerical order.
 *
 * for extra credit, have it also arrange strings in alphabetical order
 *
 * @author pr
 */
public class Challenge009Easy {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        List<Integer> digits = new ArrayList<>();
        System.out.println("Enter digits");

        while (true) {
            try {
                digits.add(s.nextInt());
                s.nextLine(); // flush enter
            } catch (Exception e) {
                break;
            }
        }

        digits.sort((Integer arg0, Integer arg1) -> arg0 - arg1);
        System.out.println(digits.toString());
    }
}
