package com.challenge009.medium;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

/**
 * Write a program that will take a string ("I LIEK CHOCOLATE MILK"), and allow the user to scan a text file for strings
 * that match. after this, allow them to replaces all instances of the string with another ("I quite enjoy chocolate
 * milk. hrmmm. yes.")
 *
 * @author pr
 */
public class Challenge009Medium {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);

        System.out.println("Enter phrase to search");
        String phrase = s.nextLine();
        System.out.println("Enter filename to be searched:");
        String filePath = s.nextLine();

        try {
            String file = Files.readString(Paths.get("src/Challenge009/classes/com/challenge003/medium/text"));

            if (file.contains(phrase)) {
                System.out.println("Phrase was found, replace it with new phrase?");
                boolean shouldReplace = s.nextBoolean();
                s.nextLine(); // flush enter

                if (shouldReplace) {
                    System.out.println("Enter new phrase");
                    String newPhrase = s.nextLine();
                    file = file.replace(phrase, newPhrase);
                    Files.writeString(Paths.get("src/Challenge009/classes/com/challenge003/medium/text"), file);
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        } catch (IOException ex) {
            System.out.println("File not found");
        }
    }
}
