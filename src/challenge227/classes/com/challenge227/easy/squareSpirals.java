package com.challenge227.easy;

import java.util.*;

public class squareSpirals {

    public static Scanner sc = new Scanner(System.in);
    private static int[][] field;
    private static int start;
    private static int max;

    public static void populateFields() {
        //Initialize Field with '+'s
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                field[i][j] = 0;
            }
        }

        // Mark Starting field
        field[start][start] = 1;
    }

    public static void calcSpiral(int number) {
        if (number < 2) {   // Account for bad input
            return;
        }

        int counter = 1;    // Keep track of how many mrore fields to go
        int steps = 1;      // How many steps to take in given direction
        int[] current = new int[]{start, start}; // Keep track of field

        while (counter < number) {

            // Right
            for (int j = 0; j < steps && counter < number; j++) {
                counter++;
                field[current[0]][current[1] + 1] = counter;
                current[1]++;    // Update X coord for current field
            }

            // Up
            for (int j = 0; j < steps && counter < number; j++) {
                counter++;
                field[current[0] - 1][current[1]] = counter;
                current[0]--;    // Update Y coord for current field
            }

            steps++;    // Take larger steps for bigger spiral

            // Left
            for (int j = 0; j < steps && counter < number; j++) {
                counter++;
                field[current[0]][current[1] - 1] = counter;
                current[1]--;    // Update X coord for current field
            }

            // Down
            for (int j = 0; j < steps && counter < number; j++) {
                counter++;
                field[current[0] + 1][current[1]] = counter;
                current[0]++;    // Update Y coord for current field
            }

            steps++;    // Take larger steps for bigger spiral
        }
    }

    public static void printFields() {

        for (int i = 0; i < field.length; i++) {

            for (int j = 0; j < 5; j++) {
                if (field[i][j] == 0) {
                    System.out.print("    +");
                } else {
                    System.out.print("    " + field[i][j]);
                }
            }
            System.out.println("\n");
        }
    }

    public static void initialize() {

        System.out.println("Enter Field dimension (n*n):");
        int dimension = sc.nextInt();
        field = new int[dimension][dimension];
        start = dimension / 2;
        max = dimension * dimension;

        System.out.println("Enter Field number to search in Spiral:");
        int number = sc.nextInt();

        populateFields();
        calcSpiral(number);
    }

    public static void main(String[] args) {
        initialize();
        printFields();
    }

}
