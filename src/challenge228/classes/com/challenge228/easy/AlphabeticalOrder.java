package com.challenge228.easy;

public class AlphabeticalOrder {

    static void isInOrder(String[] words) {
        for (String word : words) {
            boolean ordered = true;
            boolean reversed = false;

            // Find out which order the word starts with
            // Then iterate through word
            // Change the boolean status if anything changes in the order
            if (word.charAt(0) <= word.charAt(1)) {
                for (int i = 1; i < (word.length() - 1); i++) {
                    if (word.charAt(i) > word.charAt(i + 1)) {
                        ordered = false;
                        break;
                    }
                }
            } else {
                ordered = false;
                reversed = true;
                for (int i = 1; i < (word.length() - 1); i++) {
                    if (word.charAt(i) < word.charAt(i + 1)) {
                        reversed = false;
                        break;
                    }
                }
            }

            if (ordered) {
                System.out.println(word + "\tis IN ORDER.");
            } else if (reversed) {
                System.out.println(word + "\tis REVERESED.");
            } else {
                System.out.println(word + "\tis NOT IN ORDER");
            }

        }
    }

    public static void main(String[] args) {
        String[] sample_input = {"billowy", "biopsy", "chinos", "defaced", "chintz", "sponged", "bijoux", "abhors", "fiddle", "begins", "chimps", "wronged"};
        isInOrder(sample_input);
    }
}
