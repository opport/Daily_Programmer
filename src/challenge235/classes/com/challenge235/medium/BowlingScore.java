package com.challenge235.medium;

import java.util.Arrays;

public class BowlingScore {

    private static final Frame[] scorecard = new Frame[10];
    private static final int STRIKE = 10;

    public static int toInt(char ch) {
        return (int) ch - (int) '0';
    }

    public static boolean isSpare(int frame_index) {
        if (frame_index < 9) {
            if (scorecard[frame_index].getFrame()[0] != 'X') {
                if (scorecard[frame_index].getFrame()[1] == '/') {
                    return true;
                }
            }
        }
        return false;
    }

    public static int getInt(int frame_index, int char_index) {
        char ch = scorecard[frame_index].getFrame()[char_index];
        switch (ch) {
            case 'X':
                return 10;
            case '/':
                return 10 - getInt(frame_index, char_index - 1);
            case '-':
                return 0;
            default:
                return toInt(ch);
        }
    }

    public static void addStrike(int frame_index, int char_index) {
        scorecard[frame_index].addScore(STRIKE);

        // Add next two values
        if (frame_index < 9) {
            if (isSpare(frame_index + 1)) {
                scorecard[frame_index].addScore(10);
            } else if (scorecard[frame_index + 1].getFrame()[0] == 'X') {
                scorecard[frame_index].addScore(getInt(frame_index + 1, 0));
                if (frame_index + 2 < 9) { // 2nd value still in frame_bound
                    scorecard[frame_index].addScore(getInt(frame_index + 2, 0));
                } else {
                    scorecard[frame_index].addScore(getInt(frame_index + 1, 0));
                }
            } else {
                scorecard[frame_index].addScore(getInt(frame_index + 1, 0));
                scorecard[frame_index].addScore(getInt(frame_index + 1, 0));
            }
        } else if (char_index + 1 == 1) {   // Add last two values of last frame
            scorecard[frame_index].addScore(getInt(frame_index, 1));
            scorecard[frame_index].addScore(getInt(frame_index, 2));
        } else {                    // Add last value of last frame
            scorecard[frame_index].addScore(getInt(frame_index, 2));
        }
    }

    public static void addSpare(int frame_index, int char_index) {
        // Add remaining pins to score
        scorecard[frame_index].addScore(getInt(frame_index, 1));

        if (frame_index < 9) { // First value from next frame
            scorecard[frame_index].addScore(getInt(frame_index + 1, 0));
        } else if (char_index == 1) { // Add next char
            scorecard[frame_index].addScore(getInt(frame_index, char_index + 1));
        }
    }

    public static void addGutter(int frame_index, int char_index) {
        if (frame_index < 9) {
            if (char_index + 1 == 1) {
                addValue(frame_index, char_index + 1);
            }
        } else if (char_index == 0) {
            addValue(frame_index, char_index + 1);
        } else if (char_index == 1) {
            addValue(frame_index, char_index + 2);
        }
    }

    public static void addInt(char ch, int frame_index, int char_index) {
        scorecard[frame_index].addScore(toInt(ch));
        if (frame_index < 9) {
            if (char_index + 1 == 1) {
                if (isSpare(frame_index)) {
                    scorecard[frame_index].addScore(getInt(frame_index, char_index + 1));
                } else {
                    addValue(frame_index, char_index + 1);
                }
            }
        } else if (char_index + 1 == 1) {
            addValue(frame_index, char_index + 1);
        } else if (char_index + 1 == 2) {
            addValue(frame_index, char_index + 2);
        }
    }

    public static void addValue(int frame_index, int char_index) {
        char ch = scorecard[frame_index].getFrame()[char_index];
        switch (ch) {
            case 'X':
                addStrike(frame_index, char_index);
                break;
            case '/':
                addSpare(frame_index, char_index);
                break;
            case '-':
                addGutter(frame_index, char_index);
                break;
            default:
                addInt(ch, frame_index, char_index);
        }
    }

    public static void assignValues() {
        if (scorecard != null) {
            for (int i = 0; i < STRIKE; i++) {

                // Add score from previous frame
                if (i > 0) {
                    scorecard[i].addScore(scorecard[i - 1].getScore());
                }
                addValue(i, 0);
            }
        }
    }

    public static void assignChars(String str) {
        int counter = 0;
        char[] ch = new char[3];

        // Initial score acquisition
        for (int i = 0; i < 10; i++) {

            // Normal frame assignment
            if (i < 9) {
                if (str.charAt(counter) == 'X') {
                    scorecard[i] = new Frame(str.charAt(counter));
                    counter++;
                    continue;
                }
                scorecard[i] = new Frame(str.charAt(counter), str.charAt(counter + 1));
                counter += 2;
            } else { // Last frame with 3 possible attempts
                scorecard[i] = new Frame(str.charAt(counter), str.charAt(counter + 1), str.charAt(counter + 2));
                counter += 3;
            }
        }

        // Secondary score processing, taking special scores into account
        assignValues();
    }

    public static void main(String[] args) {
        // get string with without whitespaces
        String str = "X -/ X 5- 8/ 9- X 81 1- 4/X".replaceAll("\\s+", "");
        String str2 = "62 71  X 9- 8/  X  X 35 72 5/8".replaceAll("\\s+", "");
        assignChars(str);

        System.out.println(Arrays.toString(scorecard));
    }

}
