package com.challenge235.medium;

import java.util.Arrays;

/**
 * Class to keep scores in a bowling match
 *
 * @author PR
 */
public class Frame {

    private final char[] frame;
    private int score = 0;

    // Initialize frame
    public Frame(char a) {
        this.frame = new char[1];
        this.frame[0] = a;
    }

    // Initialize frame
    public Frame(char a, char b) {
        this.frame = new char[2];
        this.frame[0] = a;
        this.frame[1] = b;
    }

    // Initialize final frame
    public Frame(char a, char b, char c) {
        this.frame = new char[3];
        this.frame[0] = a;
        this.frame[1] = b;
        this.frame[2] = c;
    }

    public char[] getFrame() {
        return this.frame;
    }

    public int getScore() {
        return this.score;
    }

    public void setChars(Object value, int index) {
        if (index >= 0 && index < 4) {
            this.frame[index] = (char) value;
        }
    }

    public void addScore(int value) {
        this.score += value;
    }

    public void setScore(int value) {
        this.score = value;
    }

    @Override
    public String toString() {
        return Arrays.toString(this.frame) + "(" + this.score + ")";
    }
}
