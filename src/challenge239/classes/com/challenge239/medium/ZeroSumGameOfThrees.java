package com.challenge239.medium;

import java.util.Random;
import java.util.Scanner;

public class ZeroSumGameOfThrees {

    static int sum = 0;
    static Scanner input = new Scanner(System.in);
    static StringBuilder sb = new StringBuilder();

    public static boolean isPossible(int number) {
        if (number < 2) {
            return false;
        }
        return true;
    }

    public static int makeMove(int number) {

        if (number == 1) {
            return 1;
        } else if ((number % 3) == 0) {
            sb.append("\n").append((number / 3)).append(",sum(").append(sum).append(")").append("\t");
            System.out.print(sb);
            sb.setLength(0);
            return makeMove(number / 3);
        } else {
            int i = input.nextInt();

            // Count i as 1 upon wrong input
            // or take input and continue recursively
            if (i < -2 || i > 2 || i == 0) {
                sum += 1;
                sb.append(number + 1).append(",sum(").append(sum).append(")\t");
                System.out.print(sb);
                sb.setLength(0);
                return makeMove(number + 1);
            } else {
                sum += i;
                sb.append(number + 1).append(",sum(").append(sum).append(")\t");
                System.out.print(sb);
                sb.setLength(0);
                return makeMove(number + i);
            }
        }
    }

    public static void startGame(int number) {

        if (number == 1) {
            System.out.print("End result: " + sum);
            return;
        }

        /*
        if (!isPossible(number)) {
            System.out.println(number);
            System.out.println("Game cannot be won!");
            return;
        }
         */
        // start making moves
        int finalResult = makeMove(number);
        System.out.println("End result: " + sum);
    }

    public static void main(String[] args) {
        // Random number to start the challenge
        Random random = new Random();
        int number = random.nextInt(1000);

        System.out.print(number + ",sum(" + sum + ")\t");
        startGame(number);

    }
}
