package com.challenge239.easy;

import java.util.Random;
import java.util.Scanner;

public class GameOfThrees {

    public static int move(int remainder) {

        if (remainder == 1) {
            return 1;
        }

        if ((remainder % 3) == 0) {
            System.out.println("");
            return (remainder / 3);
        }

        Scanner input = new Scanner(System.in);
        int i = input.nextInt();

        switch (i) {
            case 1:
                return remainder + 1;
            default:
                return remainder - 1;
        }
    }

    public static void main(String[] args) {
        // Random number to start the challenge
        Random random = new Random();
        int number = random.nextInt(1000);

        while (number != 1) {
            System.out.print(number + "\t");
            number = move(number);
        }

    }
}
