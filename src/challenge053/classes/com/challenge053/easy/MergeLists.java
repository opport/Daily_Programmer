package com.challenge053.easy;

import java.util.Arrays;

public class MergeLists {
    
    public static int[] merge(int[] list1, int[] list2){
        int[] mergedList = new int[list1.length +list2.length];        
        int index_j = 0;
        int next_index = 0;
        
        for (int i = 0; i < list1.length; i++) {
            if (index_j >= list2.length) {
                mergedList[next_index] = list1[i];
                next_index++;
                break;
            }
            for (int j = index_j; j < list2.length; j++) {
                if (list1[i] < list2[j]) {
                    mergedList[next_index] = list1[i];
                    next_index++;
                    
                    // Check if list1 loop ends before list2
                    if (i+1 < list1.length && index_j < list2.length) {
                        break;
                    } else {
                        while (index_j < list2.length) {
                            mergedList[next_index] = list2[j];
                            index_j++;
                        }
                    }
                    
                } else {
                    mergedList[next_index] = list2[j];
                    index_j = j+1;    // Keep index in mind for next loop
                    next_index++;
                }
            }
        }
        
        return mergedList;
    }
    
    public static void main(String[] args) {
        int[] list1 = new int[]{1,5,7,8};
        int[] list2 = new int[]{2,3,4,7,9};
        
        System.out.println(Arrays.toString(merge(list1,list2)));
    }
}
