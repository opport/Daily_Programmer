
package com.challenge053.hard;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author PR
 * @version Expression revision is undefined on line 12, column 15 in Templates/Classes/Class.java.
 */
public class SetProduct {
    /**
     * Default list to simulate example from exercise.
     */
    final List<Integer> defaultSet = Arrays.asList(2,3,5,7,11);
    
    /**
     * Find primes p(n) as main set to find subsets.
     * @param n
     * @return 
     */
    final static List<Integer> findPrimes(Integer n) {
        // Check if number is valid
        if (n < 1) {
            return null;
        } else if (n == 1) {
            return Arrays.asList(1);
        } else {
            boolean isPrime = true;
            List<Integer> primes = new ArrayList<>();
        
            // Iterate through half of the list and return numbers divisible without a remainder.
            for (int i = 2; i <= n/2; i++) {
                for (int j = 2; j < i/2; j++) {
                    if (i%j == 0) {         // Check for prime
                        isPrime = false;    // Change isPrime flag if it is not a prime
                        break;
                    }
                }
                if (isPrime) {      // Add i if isPrime flag has not been changed
                    primes.add(i);
                }
                isPrime = true;
            }
            
            return primes;
        }
    }
    
    
    final static List<Integer> findSubset(List<Integer> set, Integer limit) {
        return set;
    }
    
    public static void main(String[] args) {
        
    }
}
