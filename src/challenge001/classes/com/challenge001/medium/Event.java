package com.challenge001.medium;

/**
 *
 * @author pr
 */
public class Event {

    String description;
    int hour;

    public Event(String description, int hour) {
        this.description = description;
        this.hour = hour;
    }
}
