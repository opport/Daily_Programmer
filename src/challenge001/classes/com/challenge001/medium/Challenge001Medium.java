package com.challenge001.medium;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * create a program that will allow you to enter events organizable by hour. There must be menu options of some form,
 * and you must be able to easily edit, add, and delete events without directly changing the source code. (note that by
 * menu i dont necessarily mean gui. as long as you can easily access the different options and receive prompts and
 * instructions telling you how to use the program, it will probably be fine)
 *
 * @author pr
 */
public class Challenge001Medium {

    private static final List<Event> events = new ArrayList<>();
    private static final Scanner s = new Scanner(System.in);

    private static void add() {
        System.out.println("Enter event name:");
        String name = s.nextLine();
        System.out.println("Enter hour:");
        int hour = s.nextInt();
        s.nextLine(); // flush newline

        if (hour < 0 || hour > 24) {
            hour = 0;
        }
        System.out.println(String.format("Added event \"%s\" at %s o clock", name, hour));

        events.add(new Event(name, hour));
    }

    private static void edit() {
        System.out.println("Choose event number:");
        for (int i = 0; i < events.size(); i++) {
            System.out.println(String.format("%s %s  at %s o clock", i, events.get(i).description, events.get(i).hour));
        }

        int number = s.nextInt();
        if (number < 0 || number >= events.size()) {
            return;
        }
        s.nextLine(); // flush newline

        System.out.println("Enter new event name:");
        String name = s.nextLine();
        System.out.println("Enter new hour:");
        int hour = s.nextInt();

        if (hour < 0 || hour > 24) {
            hour = 0;
        }
        System.out.println(String.format("Changed event #%s to \"%s\" at %s o clock", number, name, hour));

        events.set(number, new Event(name, hour));
    }

    private static void remove() {
        System.out.println("Choose event number to delete:");
        for (int i = 0; i < events.size(); i++) {
            System.out.println(String.format("%s %s %s", i, events.get(i).description, events.get(i).hour));
        }

        int number = s.nextInt();
        if (number < 0 || number >= events.size()) {
            return;
        }
        s.nextLine(); // flush newline

        System.out.println(String.format("Removed event %s \"%s\" at %s o clock", number, events.get(number).description, events.get(number).hour));

        events.remove(number);
    }

    public static void main(String[] args) {
        System.out.println("Welcome to event planner");
        System.out.println("------------------------");

        while (true) {
            System.out.println("1. Add Event");
            System.out.println("2. Edit Event");
            System.out.println("3. Delete Event");
            System.out.println("Else: Exit\n");
            int number = s.nextInt();
            if (number < 1 || number > 3) {
                break;
            }
            s.nextLine(); // flush newline

            switch (number) {
                case 1:
                    add();
                    break;
                case 2:
                    edit();
                    break;
                case 3:
                    remove();
                    break;
                default:
                    return;
            }
        }
    }
}
