package com.challenge001.hard;

import java.util.Random;
import java.util.Scanner;

/**
 * we all know the classic "guessing game" with higher or lower prompts. lets do a role reversal; you create a program
 * that will guess numbers between 1-100, and respond appropriately based on whether users say that the number is too
 * high or too low. Try to make a program that can guess your number based on user input and great code!
 *
 * @author pr
 */
public class Challenge001Hard {

    private static final Random r = new Random();
    private static final Scanner s = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Program will now guess a number:");
        System.out.println("Press l For lower guess, h for higher, or anything else to cancel");

        int upperBound = Integer.MAX_VALUE;
        int lowerBound = 0;

        while (true) {
            int lastInt = r.nextInt(upperBound) + lowerBound;
            System.out.println(String.format("%s ?", lastInt));
            String option = s.nextLine();

            switch (option) {
                case "l":
                    lowerBound = 0;
                    upperBound = lastInt / 2;
                    break;
                case "h":
                    lowerBound = lastInt;
                    upperBound = lastInt * 2;
                    break;
                default:
                    System.out.println("Exited!");
                    return;
            }
        }
    }
}
