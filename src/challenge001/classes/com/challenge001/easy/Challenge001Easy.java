package com.challenge001.easy;

import java.util.Scanner;

/**
 * create a program that will ask the users name, age, and reddit username. have it tell them the information back, in
 * the format: your name is (blank), you are (blank) years old, and your username is (blank)
 *
 * @author pr
 */
public class Challenge001Easy {

    public static void main(String[] args) {
        String name;
        int age;
        String redditName;
        Scanner s = new Scanner(System.in);

        System.out.println("Name");
        name = s.nextLine();
        System.out.println("Age");
        age = s.nextInt();
        s.nextLine();
        System.out.println("Reddit Name");
        redditName = s.nextLine();

        System.out.println(String.format("your name is %s, you are %s years old, and your username is %s", name, age, redditName));
    }
}
