package com.challenge003.easy;

import java.util.Scanner;

/**
 * Welcome to cipher day!
 *
 * write a program that can encrypt texts with an alphabetical caesar cipher. This cipher can ignore numbers, symbols,
 * and whitespace.
 *
 * for extra credit, add a "decrypt" function to your program!
 *
 * @author pr
 */
public class Challenge003Easy {

    private final static Scanner SCANNER = new Scanner(System.in);
    private final static int LOWERCASE_LOWER_BOUND = (int) 'a';
    private final static int LOWERCASE_UPPER_BOUND = (int) 'z';
    private final static int UPPERCASE_LOWER_BOUND = (int) 'A';
    private final static int UPPERCASE_UPPER_BOUND = (int) 'Z';
    private final static int INTERVAL = LOWERCASE_LOWER_BOUND - LOWERCASE_UPPER_BOUND;

    private static boolean isWithinLowercaseBounds(char c, int key, boolean encrypt) {
        return Character.isLowerCase(c)
                && encrypt
                        ? Character.isLowerCase(c + key)
                        : Character.isLowerCase(c - key);
    }

    private static boolean isWithinUppercaseBounds(char c, int key, boolean encrypt) {
        return Character.isUpperCase(c)
                && encrypt
                        ? Character.isUpperCase(c + key)
                        : Character.isUpperCase(c - key);
    }

    private static String encrypt(String s, int key) {
        char[] result = s.toCharArray();

        for (int i = 0; i < s.length(); i++) {
            if (!Character.isLetter(result[i])) {
                continue;
            }

            char c = result[i];

            if (Character.isUpperCase(c) && !Character.isUpperCase(c + key)) {
                result[i] = (char) (((c - UPPERCASE_LOWER_BOUND + key - 1) % INTERVAL) + UPPERCASE_LOWER_BOUND);

            } else if (Character.isLowerCase(c) && !Character.isLowerCase(c + key)) {
                result[i] = (char) (((c - LOWERCASE_LOWER_BOUND + key - 1) % INTERVAL) + LOWERCASE_LOWER_BOUND);
            } else {
                result[i] = (char) (c + key);
            }
        }

        return new String(result);
    }

    private static String decrypt(String s, int key) {
        char[] result = s.toCharArray();

        for (int i = 0; i < s.length(); i++) {
            if (!Character.isLetter(result[i])) {
                continue;
            }

            char c = result[i];

            if (Character.isUpperCase(c) && !Character.isUpperCase(c - key)) {
                result[i] = (char) (UPPERCASE_UPPER_BOUND + ((c - UPPERCASE_LOWER_BOUND - key + 1) % INTERVAL));
            } else if (Character.isLowerCase(c) && !Character.isLowerCase(c - key)) {
                result[i] = (char) (LOWERCASE_UPPER_BOUND + ((c - LOWERCASE_LOWER_BOUND - key + 1) % INTERVAL));
            } else {
                result[i] = (char) (c - key);
            }
        }

        return new String(result);
    }

    public static void main(String[] args) {
        System.out.println("Enter text to be encrypted:");
        String str = SCANNER.nextLine();
        System.out.println("Enter number for encryption key:");
        int key = SCANNER.nextInt();
        SCANNER.nextLine(); // flush enter

        String enc = encrypt(str, key);
        System.out.println(String.format("Encrypted: %s", enc));
        System.out.println(String.format("Decrypted: %s", decrypt(enc, key)));
    }
}
