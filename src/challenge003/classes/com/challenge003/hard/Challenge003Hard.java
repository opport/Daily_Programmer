package com.challenge003.hard;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 * Welcome to cipher day!
 *
 * For this challenge, you need to write a program that will take the scrambled words from this post, and compare them
 * against a word list (word_list file) to unscramble them. For bonus points, sort the words by length when you are
 * finished. Post your programs and/or subroutines!
 *
 * Here are your words to de-scramble:
 *
 * mkeart sleewa edcudls iragoge usrlsle nalraoci nsdeuto amrhat inknsy iferkna
 *
 * @author pr
 */
public class Challenge003Hard {

    private final static HashSet<String> WORDS = new HashSet<String>();
    private static final List<String> TO_CHECK = Arrays.asList(
            "mkeart",
            "sleewa",
            "edcudls",
            "iragoge",
            "usrlsle",
            "nalraoci",
            "nsdeuto",
            "amrhat",
            "inknsy",
            "iferkna");

    private static boolean isAnagram(String str, String other) {
        if (str.length() != other.length()) {
            return false;
        } else if (str.equals(other)) {
            return true;
        }

        List<Character> remaining = other.chars()
                .mapToObj(e -> (char) e)
                .collect(Collectors.toList());

        for (int i = 0; i < str.length(); i++) {
            int index = remaining.indexOf(str.charAt(i));

            if (index > -1) {
                remaining.remove(index);
            }
        }

        return remaining.isEmpty();
    }

    public static void main(String[] args) {

        List<String> unscrambled = new ArrayList<>();
        File file = new File("src/Challenge003/classes/com/challenge003/hard/word_list");
        try {
            Scanner s = new Scanner(file);

            while (s.hasNext()) {
                WORDS.add(s.nextLine());
            }

            TO_CHECK.forEach((str) -> {
                WORDS.stream().filter((word) -> (isAnagram(str, word))).forEachOrdered((word) -> {
                    unscrambled.add(word);
                });
            });

            unscrambled.forEach((u) -> {
                System.out.println(u);
            });
        } catch (FileNotFoundException e) {
            System.out.println(e);
        }

    }

    //Collections.sort(list, Comparator.comparing(String::length));
}
