package com.challenge003.medium;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Random;

/**
 * Welcome to cipher day!
 *
 * Create a program that can take a piece of text and encrypt it with an alphabetical substitution cipher. This can
 * ignore white space, numbers, and symbols.
 *
 * for extra credit, make it encrypt whitespace, numbers, and symbols!
 *
 * for extra extra credit, decode someone elses cipher!
 *
 * @author pr
 */
public class Challenge003Medium {

    private final static String TEST = "De finibus bonorum et malorum";
    private final static int ALPHABET_LEN = 26;
    private final static int OFFSET_CASE = 'A' - 'a';
    private final static Random R = new Random();
    private final static List<Character> ALPHABET = new ArrayList<Character>(26);
    private final static HashMap<Character, Character> CIPHER = new HashMap<>(26);
    private final static HashMap<Character, Character> CIPHER_REVERSE = new HashMap<>(26);

    public static void main(String[] args) {

        for (int i = 0; i < ALPHABET_LEN; i++) {
            ALPHABET.add((char) ('a' + i));
        }

        // assign letters to cipher
        for (int i = 0; i < ALPHABET_LEN; i++) {
            int rand = Math.abs(R.nextInt() % ALPHABET.size());
            Character c = (char) ('a' + i);

            Character removed = ALPHABET.remove(rand);

            CIPHER.put(c, removed);
            CIPHER_REVERSE.put(removed, c);
        }

        char[] result = TEST.toCharArray();

        for (int i = 0; i < TEST.length(); i++) {
            char c = result[i];

            if (CIPHER.containsKey(c)) {
                result[i] = CIPHER.get(c);
            }
        }

        System.out.println("Encrypted");
        System.out.println(result);

        for (int i = 0; i < TEST.length(); i++) {
            char c = result[i];

            if (CIPHER_REVERSE.containsKey(c)) {
                result[i] = CIPHER_REVERSE.get(c);
            }
        }

        System.out.println("Decrypted");
        System.out.println(result);

        CIPHER.entrySet().forEach((Entry<Character, Character> c) -> {
            System.out.println(String.format("%s - %s", c.getKey(), c.getValue()));
        });
    }
}
