package com.challenge222.hard;

import java.util.*;

public class DeliveryScheduling {

    private static double distance = 0;
    private final static Business myShop = new Business(40, 40, 20, 20);
    private static final Business myTruck = myShop;

    public static double calcDistance(Business current, Business destination) {
        double x = Math.pow(destination.getX() - current.getX(), 2);
        double y = Math.pow(destination.getY() - current.getY(), 2);
        return Math.sqrt(x + y);
    }

    public static void restock(Business current, Business destination) {
        distance += calcDistance(myTruck, myShop);      // Drive back to your shop
        myTruck.addUnits(myTruck.getMaxUnits() - myTruck.getUnits()); // Refill
        distance += calcDistance(myTruck, destination); // go and deliver to destination
    }

    public static void deliver(Business current, Business destination) {

        // Insufficient pieces? Return to shop for refill
        if (myTruck.getUnits() <= destination.getUnits()) {
            destination.addUnits(current.getUnits());   // Give number of pieces currently held
            current.addUnits(-current.getUnits());
            restock(current, destination);               // Drive back to restock and return
        }

        // Get required units
        destination.addUnits(destination.getMaxUnits() - destination.getUnits());
        // Update distance driven
        distance += calcDistance(current, destination);
        // Update truck position
        myTruck.setCoords(destination.getCoords());
    }

    // Delivers to shop that requires the least amount of items
    public static void greedyNaiveDelivery(ArrayList<Business> destination) {
        double dist = 0;
        Business current = myShop;

        destination.stream().forEach((b) -> {
            deliver(myTruck, b);
        });
    }

    //TODO refined greedyDelivery
    // Calculate the distances between each point 
    // then let the truck decide which delivery to make next:
    // Check if enough points are left 
    public static void greedyDelivery(ArrayList<Business> destination) {
        double dist = 0;
        Business current = myShop;

        destination.stream().forEach((b) -> {
            deliver(myTruck, b);
        });
    }

    public static void main(String[] args) {
        System.out.println(myShop);
        ArrayList<Business> sample_input = new ArrayList<>();
        sample_input.add(new Business(10, 20, 8));
        sample_input.add(new Business(15, 31, 20));
        sample_input.add(new Business(18, 13, 21));
        sample_input.add(new Business(17, 30, 20));
        sample_input.add(new Business(3, 20, 10));
        sample_input.add(new Business(5, 11, 29));
        sample_input.add(new Business(9, 28, 12));
        sample_input.add(new Business(4, 14, 14));
        sample_input.add(new Business(6, 32, 8));
        sample_input.add(new Business(12, 1, 1));
        sample_input.add(new Business(18, 3, 32));
        sample_input.add(new Business(23, 5, 5));

        Collections.sort(sample_input);

        greedyNaiveDelivery(sample_input);
        System.out.println(sample_input);
        System.out.println("Total distance travelled: " + distance);
    }
}
