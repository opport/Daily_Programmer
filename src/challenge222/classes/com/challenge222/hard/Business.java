package com.challenge222.hard;

public class Business implements Comparable<Business> {

    private Coordinates location;
    private final int maxUnits;
    private int units;

    public Business(int maxUnits, int x, int y) {
        this.location = new Coordinates(x, y);
        this.maxUnits = maxUnits;
        this.units = 0;
    }

    public Business(int maxUnits, int units, int x, int y) {
        this.location = new Coordinates(x, y);
        this.maxUnits = maxUnits;
        this.units = units;
    }

    public int getMaxUnits() {
        return this.maxUnits;
    }

    public int getUnits() {
        return this.units;
    }

    public int getX() {
        return this.location.getX();
    }

    public int getY() {
        return this.location.getY();
    }

    public Coordinates getCoords() {
        return new Coordinates(this.location.getX(), this.location.getX());
    }

    public void setX(int x) {
        this.location.setX(x);
    }

    public void setY(int y) {
        this.location.setY(y);
    }

    public void setCoords(Coordinates point) {
        this.location = point;
    }

    public void addUnits(int units) {
        if (this.units + units > this.maxUnits || this.units + units < 0) {
            System.err.println("Numberbounderror addUnits(): " + (this.maxUnits + units) + "/" + this.maxUnits);
        } else {
            this.units += units;
        }
    }

    public void deliverUnits(Business destination) {
        destination.addUnits(this.units);
        this.units = 0;
    }

    @Override
    public int compareTo(Business other) throws ClassCastException {
        if (!(other instanceof Business)) {
            throw new ClassCastException("A Person object expected.");
        }
        int otherUnits = other.getUnits();
        return this.maxUnits - otherUnits;
    }

    @Override
    public String toString() {
        return getMaxUnits() + "," + getUnits() + "," + getX() + "," + getY();
    }
}
