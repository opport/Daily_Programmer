package com.challenge222.easy;

public class BalancingWords {

    public static int toInt(char ch) {
        if ((int) ch >= (int) 'a' && (int) ch <= (int) 'z') {
            return (int) ch - (int) 'a' + 1;
        } else if ((int) ch >= (int) 'A' && (int) ch <= (int) 'Z') {
            return (int) ch - (int) 'A' + 1;
        } else {
            return (int) ch;
        }
    }

    public static int compare(String a, String b) {
        int sumA = 0, sumB = 0;

        // Add all character values multiplied by their weights 
        for (int i = 0; i < a.length(); i++) {
            sumA += toInt(a.charAt(i)) * (a.length() - i);
        }

        for (int i = 0; i < b.length(); i++) {
            sumB += toInt(b.charAt(i)) * (i + 1);
        }

        if (sumA == sumB) {
            return sumA;
        } else {
            return -1;
        }
    }

    public static void balance(String[] input) {
        String first_half;
        String second_half;

        // Check for empty input
        if (input.length == 0) {
            return;
        }

        //Iterate through all words in the list
        for (String str : input) {

            // Check special cases: Only 1 or 2 characters in string
            if (str.length() == 1) {
                System.out.println(str);
                break;
            } else if (str.length() == 2) {
                System.out.println(str + " cannot be balanced");
                break;
            }

            //Iterate balancing point through elements that belong to init && tail
            for (int i = 1; i < str.length() - 1; i++) {
                //Assign substrings to balance halves then compare differences to balance point
                first_half = str.substring(0, i);
                second_half = str.substring(i + 1, str.length());

                //Debug to see loop output
                //System.out.println(first_half + "-" + second_half);
                if (compare(first_half, second_half) != -1) {
                    System.out.println(first_half + "-" + str.charAt(i) + "-" + second_half.substring(1) + "\t" + compare(first_half, second_half));
                    break;
                } else if (i == str.length() - 2) {
                    System.out.println(str + " cannot be balanced");
                }
            }
        }
    }

    public static void main(String[] args) {
        String[] sample_input = {"CONSUBSTANTIATION",
            "WRONGHEADED",
            "UNINTELLIGIBILITY",
            "SUPERGLUE"
        };

        balance(sample_input);
    }
}
